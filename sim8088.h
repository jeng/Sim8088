#ifndef __SIM8088_H__

#define __SIM8088_H__

#define INS_STR_SZ 100
#define META_DESC_SZ 100
#define MAX_META_ENTRIES 20
#define OP_CODE_SZ 10
#define MAX_LBL 999
#define NUM_REGS 8
#define NUM_LH_REGS 8
#define NUM_SEG_REGS 4
#define MAX_MEM  (1024*1024)
#define NO_MATCH -1
#define NUM_OP_CODES 133
#define MAX_CYCLE_DETAILS 11 

#define CYCLE_EA          0x0100
#define CYCLE_CHECK_WORD  0x0200
#define CYCLE_ACCUMULATOR 0x0400
#define CYCLE_MASK        0x00ff

#define ARRAY_SIZE(x) ((sizeof x) / (sizeof *x))

enum parse_mode_t {
#define NEWPM(ParseMode) PM_##ParseMode,
#define NEWREG(...)
#define NEWLHREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

const char *parse_mode_desc[] = {
#define NEWPM(ParseMode) #ParseMode,
#define NEWREG(...)
#define NEWLHREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

enum reg_t {
#define NEWREG(reg) REG_##reg,
#define NEWPM(...)
#define NEWLHREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

const char *regs[NUM_REGS] = {
#define NEWREG(reg) #reg,
#define NEWPM(...)
#define NEWLHREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

enum lh_reg_t {
#define NEWLHREG(reg) REG_##reg,
#define NEWPM(...)
#define NEWREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

const char *lhregs[NUM_LH_REGS] = {
#define NEWLHREG(reg) #reg,
#define NEWPM(...)
#define NEWREG(...)
#define NEWSEGREG(...)

#include "sim8088_parsemodes.inl"
};

enum seg_reg_t {
#define NEWLHREG(...)
#define NEWPM(...)
#define NEWREG(...)
#define NEWSEGREG(reg, desc) REG_##reg,

#include "sim8088_parsemodes.inl"
};

const char *segregs[NUM_SEG_REGS] = {
#define NEWLHREG(...)
#define NEWPM(...)
#define NEWREG(...)
#define NEWSEGREG(reg, desc) #reg,

#include "sim8088_parsemodes.inl"
};

const char *segregsDesc[NUM_SEG_REGS] = {
#define NEWLHREG(...)
#define NEWPM(...)
#define NEWREG(...)
#define NEWSEGREG(reg, desc) desc,

#include "sim8088_parsemodes.inl"
};

enum mnemonic_t {
#define NEWOPCODE(op, ...) OP_##op,
#define OPCODE(...)

#include "sim8088_optable.inl"
};

const char *mnemonicLookup[NUM_OP_CODES] = {
#define NEWOPCODE(op, ...) #op,
#define OPCODE(...)

#include "sim8088_optable.inl"
};


struct meta_entry_t {
    char desc[META_DESC_SZ];
    bool hasValue;
    uint16_t value;
};

struct meta_data_t {
    uint32_t size;
    meta_entry_t entries[MAX_META_ENTRIES];
};

//TODO I could pack these
struct sim86_flags_t {
    bool trap;
    bool direction;
    bool interruptEnable;
    bool overflow;
    bool sign;
    bool zero;
    bool auxCarry;
    bool parity;
    bool carry;
};

enum operand_type_t{
    OT_REGISTER,
    OT_ADDRESS,
    OT_IMMEDIATE,
    OT_EFFECTIVE,
    OT_SEG_REG,
    OT_NONE
}; 

struct operand_t {
    bool isWord;
    operand_type_t type;
    uint8_t reg;
    uint16_t address;
    uint16_t immediate;
    int32_t effectiveOffset;
    uint8_t effectiveRegSize;
    uint8_t effectiveReg[2];
    uint8_t segreg;
    int32_t displacement;
};

struct ins_8088_t {
    meta_data_t meta;
    mnemonic_t mnemonic;
    char opStr[INS_STR_SZ];
    char leftStr[INS_STR_SZ];
    char rightStr[INS_STR_SZ];
    operand_t src;
    operand_t dest;
    parse_mode_t parseMode; // Needed for my label hack and prefix hack
    int length; //Length of the instruction in bytes
};

struct sim86_machine_t {
    uint16_t registers[NUM_REGS];
    uint16_t segregs[NUM_SEG_REGS];
    uint8_t *memory;
    //TODO this is instruction pointer instead of program counter on 8086
    uint16_t pc;
    size_t clock;
    sim86_flags_t flags;
    uint8_t activeSegment;
    bool blit;
    bool halt;
};

typedef void (*op_handler_t)(sim86_machine_t *machine, ins_8088_t *ins);

struct op_code_t {
    //char opcode[OP_CODE_SZ];
    mnemonic_t mnemonic;
    int8_t prefixSize;
    int16_t prefix;
    int16_t extra;
    parse_mode_t parseMode;
    op_handler_t handler;
};

struct mod_offset_t {
    bool isWord;
    int16_t word;
    uint8_t low;
    uint8_t high;
};

struct unpack_byte_t {
    uint8_t size;
    uint8_t fields[8];
};

//TODO this will probably change to code_segment_t
struct memory_segment_t {
    uint8_t *data;
    uint16_t size;
    uint16_t offset;
    uint16_t startAddress;
};

struct mod_info_t {
    uint8_t mod;
    uint8_t reg;
    uint8_t rm;
    mod_offset_t offset;
};

struct cycle_details_t {
    operand_type_t dest;
    operand_type_t src;
    bool isWord;
    uint16_t cycles;
    uint8_t transfers;
};

struct cycle_info_t {
    mnemonic_t mnemonic;
    uint8_t detailCount;
    cycle_details_t details[MAX_CYCLE_DETAILS];
};

static ins_8088_t Parse8088(memory_segment_t *feed);
static void ToBinaryString(uint8_t x, char *buffer);

/* machine definitions */
sim86_machine_t BuildMachine();
void MovEval(sim86_machine_t *machine, ins_8088_t *ins);
void AddEval(sim86_machine_t *machine, ins_8088_t *ins);
void AdcEval(sim86_machine_t *machine, ins_8088_t *ins);
void SubEval(sim86_machine_t *machine, ins_8088_t *ins);
void SbbEval(sim86_machine_t *machine, ins_8088_t *ins);
void CmpEval(sim86_machine_t *machine, ins_8088_t *ins);
void JnbEval(sim86_machine_t *machine, ins_8088_t *ins);
void JnbeEval(sim86_machine_t *machine, ins_8088_t *ins);
void JoEval(sim86_machine_t *machine, ins_8088_t *ins);
void JnoEval(sim86_machine_t *machine, ins_8088_t *ins);
void JbEval(sim86_machine_t *machine, ins_8088_t *ins);
void JeEval(sim86_machine_t *machine, ins_8088_t *ins);
void JneEval(sim86_machine_t *machine, ins_8088_t *ins);
void JbeEval(sim86_machine_t *machine, ins_8088_t *ins);
void JsEval(sim86_machine_t *machine, ins_8088_t *ins);
void JnsEval(sim86_machine_t *machine, ins_8088_t *ins);
void JpEval(sim86_machine_t *machine, ins_8088_t *ins);
void JnpEval(sim86_machine_t *machine, ins_8088_t *ins);
void JlEval(sim86_machine_t *machine, ins_8088_t *ins);
void JgeEval(sim86_machine_t *machine, ins_8088_t *ins);
void JleEval(sim86_machine_t *machine, ins_8088_t *ins);
void JgEval(sim86_machine_t *machine, ins_8088_t *ins);
void LoopnzEval(sim86_machine_t *machine, ins_8088_t *ins);
void LoopzEval(sim86_machine_t *machine, ins_8088_t *ins);
void LoopEval(sim86_machine_t *machine, ins_8088_t *ins);
void PushEval(sim86_machine_t *machine, ins_8088_t *ins);
void PopEval(sim86_machine_t *machine, ins_8088_t *ins);
void PrefixEval(sim86_machine_t *machine, ins_8088_t *ins);
void WaitEval(sim86_machine_t *machine, ins_8088_t *ins);
void JmpEval(sim86_machine_t *machine, ins_8088_t *ins);
void CallEval(sim86_machine_t *machine, ins_8088_t *ins);
void RetEval(sim86_machine_t *machine, ins_8088_t *ins);
void HaltEval(sim86_machine_t *machine, ins_8088_t *ins);
void MulEval(sim86_machine_t *machine, ins_8088_t *ins);
void IncEval(sim86_machine_t *machine, ins_8088_t *ins);
void ShlEval(sim86_machine_t *machine, ins_8088_t *ins);
void XorEval(sim86_machine_t *machine, ins_8088_t *ins);
void AndEval(sim86_machine_t *machine, ins_8088_t *ins);
void OrEval(sim86_machine_t *machine, ins_8088_t *ins);
void CbwEval(sim86_machine_t *machine, ins_8088_t *ins);


static op_code_t opTable[NUM_OP_CODES] = {
#define NEWOPCODE(op, prefixSize, prefix, extra, parseMode, handler) {OP_##op, prefixSize, prefix, extra, parseMode, handler},
#define OPCODE(op, prefixSize, prefix, extra, parseMode, handler) {OP_##op, prefixSize, prefix, extra, parseMode, handler},

#include "sim8088_optable.inl"
};

#endif /*__SIM8088_H__*/
