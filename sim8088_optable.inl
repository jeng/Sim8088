NEWOPCODE(ADD,    6, 0x00, NO_MATCH, PM_REG_MEM_W_REG, AddEval)
   OPCODE(ADD,    6, 0x20,     0x00, PM_IMM_REG_MEM, AddEval)
   OPCODE(ADD,    7, 0x02, NO_MATCH, PM_IMM_ACC, AddEval)
NEWOPCODE(ADC,    6, 0x04, NO_MATCH, PM_REG_MEM_W_REG, AdcEval)
   OPCODE(ADC,    6, 0x20,     0x02, PM_IMM_REG_MEM, AdcEval)
   OPCODE(ADC,    7, 0x0a, NO_MATCH, PM_IMM_ACC, AdcEval)
NEWOPCODE(SUB,    6, 0x0a, NO_MATCH, PM_REG_MEM_W_REG, SubEval)
   OPCODE(SUB,    6, 0x20,     0x05, PM_IMM_REG_MEM, SubEval)
   OPCODE(SUB,    7, 0x16, NO_MATCH, PM_IMM_ACC, SubEval)
NEWOPCODE(SBB,    6, 0x06, NO_MATCH, PM_REG_MEM_W_REG, SbbEval)
   OPCODE(SBB,    6, 0x20,     0x03, PM_IMM_REG_MEM, SbbEval)
   OPCODE(SBB,    7, 0x0e, NO_MATCH, PM_IMM_ACC, SbbEval)
NEWOPCODE(CMP,    6, 0x0e, NO_MATCH, PM_REG_MEM_W_REG, CmpEval)
   OPCODE(CMP,    6, 0x20,     0x07, PM_IMM_REG_MEM, CmpEval)
   OPCODE(CMP,    7, 0x1e, NO_MATCH, PM_IMM_ACC, CmpEval)
NEWOPCODE(JO,     8, 0x70, NO_MATCH, PM_LABEL, JoEval)
NEWOPCODE(JNO,    8, 0x71, NO_MATCH, PM_LABEL, JnoEval)
NEWOPCODE(JB,     8, 0x72, NO_MATCH, PM_LABEL, JbEval)
NEWOPCODE(JNB,    8, 0x73, NO_MATCH, PM_LABEL, JnbEval)
NEWOPCODE(JE,     8, 0x74, NO_MATCH, PM_LABEL, JeEval)
NEWOPCODE(JNE,    8, 0x75, NO_MATCH, PM_LABEL, JneEval)
NEWOPCODE(JBE,    8, 0x76, NO_MATCH, PM_LABEL, JbeEval)
NEWOPCODE(JNBE,   8, 0x77, NO_MATCH, PM_LABEL, JnbeEval)
NEWOPCODE(JS,     8, 0x78, NO_MATCH, PM_LABEL, JsEval)
NEWOPCODE(JNS,    8, 0x79, NO_MATCH, PM_LABEL, JnsEval)
NEWOPCODE(JP,     8, 0x7a, NO_MATCH, PM_LABEL, JpEval)
NEWOPCODE(JNP,    8, 0x7b, NO_MATCH, PM_LABEL, JnpEval)
NEWOPCODE(JL,     8, 0x7c, NO_MATCH, PM_LABEL, JlEval)
NEWOPCODE(JGE,    8, 0x7d, NO_MATCH, PM_LABEL, JgeEval)
NEWOPCODE(JLE,    8, 0x7e, NO_MATCH, PM_LABEL, JleEval)
NEWOPCODE(JG,     8, 0x7f, NO_MATCH, PM_LABEL, JgEval)
NEWOPCODE(LOOPNZ, 8, 0xe0, NO_MATCH, PM_LABEL, LoopnzEval)
NEWOPCODE(LOOPZ,  8, 0xe1, NO_MATCH, PM_LABEL, LoopzEval)
NEWOPCODE(LOOP,   8, 0xe2, NO_MATCH, PM_LABEL, LoopEval)
NEWOPCODE(JCXZ,   8, 0xe3, NO_MATCH, PM_LABEL, NULL)
NEWOPCODE(PUSH,   8, 0xff,     0x06, PM_REG_MEM, PushEval)
   OPCODE(PUSH,   5, 0x0a, NO_MATCH, PM_REG, PushEval)
   OPCODE(PUSH,  -3, 0x06, NO_MATCH, PM_SEG_REG, PushEval)
NEWOPCODE(POP,    8, 0x8f,     0x00, PM_REG_MEM, PopEval)
   OPCODE(POP,    5, 0x0b, NO_MATCH, PM_REG, PopEval)
   OPCODE(POP,   -3, 0x07, NO_MATCH, PM_SEG_REG, PopEval)
NEWOPCODE(XCHG,   7, 0x43, NO_MATCH, PM_REG_MEM_W_REG, NULL)
   OPCODE(XCHG,   5, 0x12, NO_MATCH, PM_REG_W_ACC, NULL)
NEWOPCODE(IN,     7, 0x72, NO_MATCH, PM_FIXED_PORT, NULL)
   OPCODE(IN,     7, 0x76, NO_MATCH, PM_VAR_PORT, NULL)
NEWOPCODE(OUT,    7, 0x73, NO_MATCH, PM_FIXED_PORT, NULL)
   OPCODE(OUT,    7, 0x77, NO_MATCH, PM_VAR_PORT, NULL)
NEWOPCODE(XLAT,   8, 0xd7, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(LEA,    8, 0x8d, NO_MATCH, PM_NO_DIR_REG_MEM_W_REG, NULL)
NEWOPCODE(LDS,    8, 0xc5, NO_MATCH, PM_NO_DIR_REG_MEM_W_REG, NULL)
NEWOPCODE(LES,    8, 0xc4, NO_MATCH, PM_NO_DIR_REG_MEM_W_REG, NULL)
NEWOPCODE(LAHF,   8, 0x9f, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(SAHF,   8, 0x9e, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(PUSHF,  8, 0x9c, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(POPF,   8, 0x9d, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(AAA,    8, 0x37, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(DAA,    8, 0x27, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(INC,    7, 0x7f,     0x00, PM_REG_MEM, IncEval)
   OPCODE(INC,    5, 0x08, NO_MATCH, PM_REG, IncEval)
NEWOPCODE(DEC,    7, 0x7f,     0x01, PM_REG_MEM, NULL/*DecEval*/)
   OPCODE(DEC,    5, 0x09, NO_MATCH, PM_REG, NULL/*DecEval*/)
NEWOPCODE(NEG,    7, 0x7b,     0x03, PM_REG_MEM, NULL)
NEWOPCODE(AAS,    8, 0x3f, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(DAS,    8, 0x2f, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(MUL,    7, 0x7b,     0x04, PM_REG_MEM, MulEval)
NEWOPCODE(IMUL,   7, 0x7b,     0x05, PM_REG_MEM, NULL)
NEWOPCODE(DIV,    7, 0x7b,     0x06, PM_REG_MEM, NULL/*DivEval*/)
NEWOPCODE(IDIV,   7, 0x7b,     0x07, PM_REG_MEM, NULL)
NEWOPCODE(CBW,    8, 0x98, NO_MATCH, PM_SINGLE, CbwEval)
NEWOPCODE(CWD,    8, 0x99, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(NOT,    7, 0x7b,     0x02, PM_REG_MEM, NULL)
NEWOPCODE(SHL,    6, 0x34,     0x04, PM_REG_MEM_V, ShlEval)
NEWOPCODE(SHR,    6, 0x34,     0x05, PM_REG_MEM_V, NULL)
NEWOPCODE(SAR,    6, 0x34,     0x07, PM_REG_MEM_V, NULL)
NEWOPCODE(ROL,    6, 0x34,     0x00, PM_REG_MEM_V, NULL)
NEWOPCODE(ROR,    6, 0x34,     0x01, PM_REG_MEM_V, NULL)
NEWOPCODE(RCL,    6, 0x34,     0x02, PM_REG_MEM_V, NULL)
NEWOPCODE(RCR,    6, 0x34,     0x03, PM_REG_MEM_V, NULL)
NEWOPCODE(AND,    6, 0x08, NO_MATCH, PM_REG_MEM_W_REG, AndEval)
   OPCODE(AND,    6, 0x20,     0x04, PM_IMM_REG_MEM, AndEval)
   OPCODE(AND,    7, 0x12, NO_MATCH, PM_IMM_ACC, AndEval)
NEWOPCODE(TEST,   6, 0x21, NO_MATCH, PM_REG_MEM_W_REG, NULL)
   OPCODE(TEST,   7, 0x7b,     0x00, PM_IMM_REG_MEM_NO_SIGN, NULL)//thisone 
   OPCODE(TEST,   7, 0x54, NO_MATCH, PM_IMM_ACC, NULL)
NEWOPCODE(OR,     6, 0x02, NO_MATCH, PM_REG_MEM_W_REG, OrEval)
   OPCODE(OR,     6, 0x20,     0x01, PM_IMM_REG_MEM, OrEval)
   OPCODE(OR,     7, 0x06, NO_MATCH, PM_IMM_ACC, OrEval)
NEWOPCODE(XOR,    6, 0x0c, NO_MATCH, PM_REG_MEM_W_REG, XorEval)
   OPCODE(XOR,    6, 0x20,     0x06, PM_IMM_REG_MEM, XorEval)
   OPCODE(XOR,    7, 0x1a, NO_MATCH, PM_IMM_ACC, XorEval)
NEWOPCODE(REP,    7, 0x79, NO_MATCH, PM_REP, NULL)
NEWOPCODE(CLC,    8, 0xf8, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(STC,    8, 0xf9, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(CLI,    8, 0xfa, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(STI,    8, 0xfb, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(CLD,    8, 0xfc, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(STD,    8, 0xfd, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(LOCK,   8, 0xf0, NO_MATCH, PM_PREFIX, NULL)
NEWOPCODE(WAIT,   8, 0x9b, NO_MATCH, PM_SINGLE, WaitEval)
NEWOPCODE(HLT,    8, 0xf4, NO_MATCH, PM_SINGLE, HaltEval)
NEWOPCODE(CMC,    8, 0xf5, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(CALL,   8, 0xe8, NO_MATCH, PM_DIRECT_IN_SEG, CallEval)
   OPCODE(CALL,   8, 0xff,     0x02, PM_REG_MEM, CallEval)
   OPCODE(CALL,   8, 0xff,     0x03, PM_INDIRECT_INTERSEGMENT, CallEval)
   OPCODE(CALL,   8, 0x9a, NO_MATCH, PM_DIRECT_INTERSEGMENT, CallEval)
NEWOPCODE(JMP,    8, 0xe9, NO_MATCH, PM_DIRECT_IN_SEG, JmpEval)
   OPCODE(JMP,    8, 0xff,     0x04, PM_REG_MEM, JmpEval)
   OPCODE(JMP,    8, 0xff,     0x05, PM_INDIRECT_INTERSEGMENT, JmpEval)
   OPCODE(JMP,    8, 0xea, NO_MATCH, PM_DIRECT_INTERSEGMENT, JmpEval)
   OPCODE(JMP,    8, 0xeb, NO_MATCH, PM_DIRECT_IN_SEG_SHORT, JmpEval)
NEWOPCODE(RET,    7, 0x61, NO_MATCH, PM_ONE_WORD, RetEval)
NEWOPCODE(RETF,   7, 0x65, NO_MATCH, PM_ONE_WORD, NULL)
NEWOPCODE(INT,    7, 0x66, NO_MATCH, PM_TYPE_SPEC, NULL)
NEWOPCODE(INTO,   8, 0xce, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(IRET,   8, 0xcf, NO_MATCH, PM_SINGLE, NULL)
//NEWOPCODE(IRET,   8, 0xcf, NO_MATCH, PM_SINGLE, NULL)
NEWOPCODE(CS,     8, 0x2e, NO_MATCH, PM_PREFIX, PrefixEval)
NEWOPCODE(DS,     8, 0x3e, NO_MATCH, PM_PREFIX, PrefixEval)
NEWOPCODE(ES,     8, 0x26, NO_MATCH, PM_PREFIX, PrefixEval)
NEWOPCODE(SS,     8, 0x36, NO_MATCH, PM_PREFIX, PrefixEval)
NEWOPCODE(AAM,    8, 0xd4, NO_MATCH, PM_DOUBLE, NULL)
NEWOPCODE(AAD,    8, 0xd5, NO_MATCH, PM_DOUBLE, NULL)
NEWOPCODE(MOV,    6, 0x22, NO_MATCH, PM_REG_MEM_W_REG,       MovEval)
   OPCODE(MOV,    4, 0x0b, NO_MATCH, PM_IMM_REG,             MovEval)
   OPCODE(MOV,    7, 0x63,     0x00, PM_IMM_REG_MEM_NO_SIGN, MovEval)
   OPCODE(MOV,    6, 0x28, NO_MATCH, PM_IMM_ACC_DIR,         MovEval)
   OPCODE(MOV,    8, 0x8c, NO_MATCH, PM_REG_MEM_SEG_REG,     MovEval)
   OPCODE(MOV,    8, 0x8e, NO_MATCH, PM_REG_MEM_SEG_REG,     MovEval)
NEWOPCODE(MOVS,   7, 0x52, NO_MATCH, PM_STRING_MAN, NULL)
NEWOPCODE(CMPS,   7, 0x53, NO_MATCH, PM_STRING_MAN, NULL)
NEWOPCODE(STOS,   7, 0x55, NO_MATCH, PM_STRING_MAN, NULL)
NEWOPCODE(LODS,   7, 0x56, NO_MATCH, PM_STRING_MAN, NULL)
NEWOPCODE(SCAS,   7, 0x57, NO_MATCH, PM_STRING_MAN, NULL)

#undef NEWOPCODE
#undef OPCODE

// current table
//
//mov    1000 1000              PM_REG_MEM_W_REG        (Register/memory to/from register)
//mov    1011 0000              PM_IMM_REG              (immediate to register)
//mov    1100 0110   0000 0000  PM_IMM_REG_MEM_NO_SIGN  (immediate to register/memory)
//mov    1010 0000              PM_IMM_ACC_DIR          (memory to accumulator) & (accumulator to memory)
//push   1111 1111   0000 0110  PM_REG_MEM              (register/memory)
//push   0101 0000              PM_REG                  (register)
//push   0000 0000              PM_SEG_REG              (segment register)
//pop    1000 1111   0000 0000  PM_REG_MEM
//pop    0101 1000              PM_REG
//pop    0000 0000              PM_SEG_REG
//xchg   1000 0110              PM_REG_MEM_W_REG       (register/memory with register)
//xchg   1001 0000              PM_REG_W_ACC           (register with accumulator)
//in     1110 0100              PM_FIXED_PORT
//in     1110 1100              PM_VAR_PORT
//out    1110 0110              PM_FIXED_PORT
//out    1110 1110              PM_VAR_PORT
//add    0000 0000              PM_REG_MEM_W_REG       (register/memory with register to either)
//add    1000 0000   0000 0000  PM_IMM_REG_MEM         (immediate to register/memory)
//add    0000 0100              PM_IMM_ACC             (immediate to accumulator)
//adc    0001 0000              PM_REG_MEM_W_REG
//adc    1000 0000   0000 0010  PM_IMM_REG_MEM
//adc    0001 0100              PM_IMM_ACC
//sub    0010 1000              PM_REG_MEM_W_REG
//sub    1000 0000   0000 0101  PM_IMM_REG_MEM
//sub    0010 1100              PM_IMM_ACC
//sbb    0001 1000              PM_REG_MEM_W_REG
//sbb    1000 0000   0000 0011  PM_IMM_REG_MEM
//sbb    0001 1100              PM_IMM_ACC
//cmp    0011 1000              PM_REG_MEM_W_REG
//cmp    1000 0000   0000 0111  PM_IMM_REG_MEM
//cmp    0011 1100              PM_IMM_ACC
//jo     0111 0000              PM_LABEL
//jno    0111 0001              PM_LABEL
//jb     0111 0010              PM_LABEL
//jnb    0111 0011              PM_LABEL
//je     0111 0100              PM_LABEL
//jne    0111 0101              PM_LABEL
//jbe    0111 0110              PM_LABEL
//ja     0111 0111              PM_LABEL
//js     0111 1000              PM_LABEL
//jns    0111 1001              PM_LABEL
//jpe    0111 1010              PM_LABEL
//jpo    0111 1011              PM_LABEL
//jl     0111 1100              PM_LABEL
//jge    0111 1101              PM_LABEL
//jle    0111 1110              PM_LABEL
//jg     0111 1111              PM_LABEL
//loopn  1110 0000              PM_LABEL
//loopz  1110 0001              PM_LABEL
//loop   1110 0010              PM_LABEL
//jcxz   1110 0011              PM_LABEL
//xlat   1101 0111              PM_SINGLE
//lea    1000 1101              PM_NO_DIR_REG_MEM_W_REG
//lds    1100 0101              PM_NO_DIR_REG_MEM_W_REG
//les    1100 0100              PM_NO_DIR_REG_MEM_W_REG
//lahf   1001 1111              PM_SINGLE
//sahf   1001 1110              PM_SINGLE
//pushf  1001 1100              PM_SINGLE
//popf   1001 1101              PM_SINGLE
//aaa    0011 0111              PM_SINGLE
//daa    0010 0111              PM_SINGLE
//inc    1111 1110   0000 0000  PM_REG_MEM            (register/memory)
//inc    0100 0000              PM_REG                (register)
//dec    1111 1110   0000 0001  PM_REG_MEM
//dec    0100 1000              PM_REG
//neg    1111 0110   0000 0011  PM_REG_MEM
//aas    0011 1111              PM_SINGLE
//das    0010 1111              PM_SINGLE
//mul    1111 0110   0000 0100  PM_REG_MEM
//imul   1111 0110   0000 0101  PM_REG_MEM
//div    1111 0110   0000 0110  PM_REG_MEM
//idiv   1111 0110   0000 0111  PM_REG_MEM
//cbw    1001 1000              PM_SINGLE
//cwd    1001 1001              PM_SINGLE
//not    1111 0110   0000 0010  PM_REG_MEM
//shl    1101 0000   0000 0100  PM_REG_MEM_V
//shr    1101 0000   0000 0101  PM_REG_MEM_V
//sar    1101 0000   0000 0111  PM_REG_MEM_V
//rol    1101 0000   0000 0000  PM_REG_MEM_V
//ror    1101 0000   0000 0001  PM_REG_MEM_V
//rcl    1101 0000   0000 0010  PM_REG_MEM_V
//rcr    1101 0000   0000 0011  PM_REG_MEM_V
//and    0010 0000              PM_REG_MEM_W_REG
//and    1000 0000   0000 0100  PM_IMM_REG_MEM
//and    0010 0100              PM_IMM_ACC
//test   1000 0100              PM_REG_MEM_W_REG
//test   1111 0110   0000 0000  PM_IMM_REG_MEM
//test   1010 1000              PM_IMM_ACC
//or     0000 1000              PM_REG_MEM_W_REG
//or     1000 0000   0000 0001  PM_IMM_REG_MEM
//or     0000 1100              PM_IMM_ACC
//xor    0011 0000              PM_REG_MEM_W_REG
//xor    1000 0000   0000 0110  PM_IMM_REG_MEM
//xor    0011 0100              PM_IMM_ACC
//rep    1111 0010              PM_REP
//clc    1111 1000              PM_SINGLE
//stc    1111 1001              PM_SINGLE
//cli    1111 1010              PM_SINGLE
//sti    1111 1011              PM_SINGLE
//cld    1111 1100              PM_SINGLE
//std    1111 1101              PM_SINGLE
//lock   1111 0000              PM_PREFIX
//wait   1001 1011              PM_SINGLE
//hlt    1111 0100              PM_SINGLE
//cmc    1111 0101              PM_SINGLE
//call   1110 1000              PM_DIRECT_IN_SEG       (direct within segment)
//call   1111 1111   0000 0010  PM_REG_MEM
//jmp    1110 1001              PM_DIRECT_IN_SEG
//jmp    1111 1111   0000 0100  PM_REG_MEM
//ret    1100 0010              PM_ONE_WORD
//int    1100 1101              PM_TYPE_SPEC
//int    1100 1100              PM_SINGLE
//into   1100 1110              PM_SINGLE
//iret   1100 1111              PM_SINGLE
//cs     0010 1110              PM_PREFIX
//ds     0011 1110              PM_PREFIX
//es     0010 0110              PM_PREFIX
//ss     0011 0110              PM_PREFIX
//iret   1100 1111              PM_SINGLE
//aam    1101 0100              PM_DOUBLE
//aad    1101 0101              PM_DOUBLE


