# sim8088

Casey Muratori is running a course on [Performance-Aware
Programming](https://www.computerenhance.com/p/performance-aware-programming-series).
Part of the initial homework was to decode a small set of instructions for the
Intel 8088.  I took this as an opportunity to learn ImGui since I wanted to be
able to visualize the parsing of the instructions.  

This program will let you pick a binary file assembled with `nasm`,
something like [listing
40](https://github.com/cmuratori/computer_enhance/blob/main/perfaware/part1/listing_0040_challenge_movs.asm) and disassemble it.

Checking the box next to the disassembled line will show metadata collected
while parsing the instruction and its location in the instruction stream.  The
location is highlighted in yellow in the binary display window.

![Example](https://post.lurk.org/system/media_attachments/files/109/984/902/724/336/657/original/b16bb90329c06671.mp4)

![bin screenshot](screenshot.jpg)
![hex screenshot](screenshot2.jpg)
![mem screenshot](screenshot3.jpg)

## Building and running on windows
### Update the CMakeLists.txt

Under the `if (WIN32)` section update the location of SDL and SDL headers.
I've included glad but if you have your own update that too.

### Building the make file

Run `cmake CMakeLists.txt`

### Building the executable

Run `msbuild ALL_BUILD.vcxproj`

### Running the program

Copy the `SDL.dll` and `verdana.tff` into the direction with the exe.  This
could be the `Debug` or the `Release` folder.  Start the program with that
folder as the working directory.

## Building and running on linux

### Update the CMakeLists.txt

Under the `if (UNIX)` section update the location of `glad`.

### Building the make file

Run `cmake CMakeLists.txt`

### Building the executable

Run `make`

### Running the program

Start the program `./sim8088`


## TODO
 - Add a cross platform file dialog for open and save
 - Add function menu to launch diff, reassemble, etc...

## References

This code was started from the ImGui SDL example provided by:

An example of using [Dear ImGui](https://github.com/ocornut/imgui) with [SDL](https://www.libsdl.org).
More information in the following [article](https://retifrav.github.io/blog/2019/05/26/sdl-imgui/).

