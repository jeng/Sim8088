void GetRegStr(uint8_t reg, uint8_t w, char output[INS_STR_SZ], operand_t *operand){
    if (0 <= reg && reg <= 7){
        operand->isWord = w;
        if (w){
           strncpy(output, regs[reg], INS_STR_SZ);
        } else {
            strncpy(output, lhregs[reg], INS_STR_SZ);
        }
        operand->type = OT_REGISTER;
        operand->reg = reg;
    } else {
        strncpy(output, "??", INS_STR_SZ);
    }
}

void SetEffectiveAddress(uint8_t rm, int32_t offset, operand_t *operand){

    operand->effectiveRegSize = (rm > 3) ? 1 : 2;
    operand->effectiveOffset = offset;

    switch(rm){
        case 0://"[bx+si%s]"
            operand->effectiveReg[0] = REG_BX;
            operand->effectiveReg[1] = REG_SI;
            break;
        case 1://"[bx+di%s]"
            operand->effectiveReg[0] = REG_BX;
            operand->effectiveReg[1] = REG_DI;
            break;
        case 2://"[bp+si%s]"
            operand->effectiveReg[0] = REG_BP;
            operand->effectiveReg[1] = REG_SI;
            break;
        case 3://"[bp+di%s]"
            operand->effectiveReg[0] = REG_BP;
            operand->effectiveReg[1] = REG_DI;
            break;
        case 4://"[si%s]"
            operand->effectiveReg[0] = REG_SI;
            break;
        case 5://"[di%s]"
            operand->effectiveReg[0] = REG_DI;
            break;
        case 6://"[bp%s]"
            operand->effectiveReg[0] = REG_BP;
            break;
        case 7://"[bx%s]"};
            operand->effectiveReg[0] = REG_BX;
            break;
    }
    
    operand->type = OT_ADDRESS;
}

void GetModStr(mod_info_t modinfo, uint8_t w, char output[INS_STR_SZ], operand_t *operand){
    const char *mods[] = {
        "[bx+si%s]", "[bx+di%s]", "[bp+si%s]", "[bp+di%s]", "[si%s]", 
        "[di%s]", "[bp%s]", "[bx%s]"};
 
    int16_t offset = modinfo.offset.word;

    if (modinfo.rm < 0 || 7 < modinfo.rm){
        strncpy(output, "??", INS_STR_SZ);
        return;
    }

    if (modinfo.mod == 0){
        if (modinfo.rm == 6){
            sprintf(output, "[%d]", offset);
            operand->address = offset;
            operand->type = OT_ADDRESS;
        } else {
            sprintf(output, mods[modinfo.rm], "");
            SetEffectiveAddress(modinfo.rm, 0, operand);
        }
    } else if (modinfo.mod == 1 || modinfo.mod == 2){
        char s[100] = {0};
        if (offset != 0){
            if (offset > 0)
                sprintf(s, "+%d", offset);
            else
                sprintf(s, "%d", offset);
        }
        sprintf(output, mods[modinfo.rm], s);
        SetEffectiveAddress(modinfo.rm, offset, operand);
    } else if (modinfo.mod == 3 && w == 0){
        GetRegStr(modinfo.rm, 0, output, operand);
    } else { //if (mod == 3 && w == 1) 
        GetRegStr(modinfo.rm, 1, output, operand);
    }

    operand->isWord = w;
}

void GetSegRegStr(uint8_t reg, char *output, operand_t *operand){
    if (0 <= reg && reg <= NUM_SEG_REGS){
        operand->segreg = reg;
        operand->type = OT_SEG_REG;
        strncpy(output, segregs[reg], INS_STR_SZ);
    } else {
        strncpy(output, "??", INS_STR_SZ);
    }
}

int16_t PackWord(uint8_t low, uint8_t high){
    int16_t word = high;
    word = word << 8;
    word = word & 0xff00;
    word = word | (low & 0xff);
    return word;
}

mod_offset_t GetModOffset(memory_segment_t *feed, uint8_t mod, uint8_t rm){
    mod_offset_t result = {0};
    if ((mod == 0 && rm == 6) || mod == 2){
        result.low  = feed->data[feed->offset++];
        result.high = feed->data[feed->offset++];
        result.word = PackWord(result.low, result.high);
        result.isWord = true;
    } else if (mod == 1){
        int8_t disp1 = feed->data[feed->offset++];
        result.low = disp1;
        result.word = disp1;
        result.isWord = false;
    }
    return result;
}

unpack_byte_t UnpackByte(uint8_t byte, uint8_t numFields, ...){
    va_list argp;
    unpack_byte_t result = {0};
    result.size = numFields;
    va_start(argp, numFields);
    int lastShift = 8;
    for(int i = 0; i < numFields; i++){
        int length = va_arg(argp, int);
        uint8_t mask = (uint8_t)(1 << length) - 1;
        uint8_t shift = lastShift - length;
        lastShift = shift;
        mask <<= shift;
        result.fields[i] = (byte & mask) >> shift;
    }
    va_end(argp);
    return result;
}

bool MatchPrefix(uint8_t byte, int16_t size, int16_t prefix){
    
    if (size < 0){

        //Another kludgey edge case for segment push and pop.  I would need to
        //encode another byte for every op to have this in the table

        size = size * -1;
        uint8_t mask = (uint8_t)(1 << size) - 1;
        uint8_t value = (byte & mask);
        uint8_t mask2 = 0xe0;
        uint8_t value2 = (byte & mask2);
        return (value == prefix && value2 == 0);
    }

    uint8_t mask = (uint8_t)(1 << size) - 1;
    uint8_t shift = 8 - size;
    mask <<= shift;
    uint8_t value = (byte & mask) >> shift;

    return (value == prefix);
}

mod_info_t UnpackModInfo(memory_segment_t *feed){
    mod_info_t info;
    unpack_byte_t bits = UnpackByte(
        feed->data[feed->offset++], 
        3, 
        2, 3, 3);//mod reg r/m
    
    info.mod = bits.fields[0];
    info.reg = bits.fields[1];
    info.rm  = bits.fields[2];

    info.offset = GetModOffset(feed, info.mod, info.rm);

    return info;
}

#define SET_META_DATA(idx, _hasValue, _desc, _value)\
        {memset(ins.meta.entries[idx].desc, 0, META_DESC_SZ);\
        ins.meta.entries[idx].value = (_value);\
        strncpy(ins.meta.entries[idx].desc, (_desc), META_DESC_SZ);\
        ins.meta.entries[idx++].hasValue = (_hasValue);}

#define SET_META_DATAP(idx, _hasValue, _desc, _value)\
        {memset(ins->meta.entries[*idx].desc, 0, META_DESC_SZ);\
        ins->meta.entries[*idx].value = (_value);\
        strncpy(ins->meta.entries[*idx].desc, (_desc), META_DESC_SZ);\
        ins->meta.entries[(*idx)++].hasValue = (_hasValue);}
        
void SetMetaModInfo(int *metaIdx, ins_8088_t *ins, mod_info_t info){
    SET_META_DATAP(metaIdx, true, "mod", info.mod);
    SET_META_DATAP(metaIdx, true, "reg", info.reg);
    SET_META_DATAP(metaIdx, true, "r/m", info.rm);
    if (info.offset.isWord){
        SET_META_DATAP(metaIdx, true, "16 bit disp(low)", info.offset.low);
        SET_META_DATAP(metaIdx, true, "16 bit disp(high)", info.offset.high);
    } else {
        SET_META_DATAP(metaIdx, true, "8 bit disp", info.offset.low);
    }
}


/*
 *                  \||/
 *                  |  @___oo
 *        /\  /\   / (__,,,,|
 *       ) /^\) ^\/ _)
 *       )   /^\/   _)
 *       )   _ /  / _)
 *   /\  )/\/ ||  | )_)
 *  <  >      |(,,) )__)
 *   ||      /    \)___)\
 *   | \____(      )___) )___
 *    \______(_______;;; __;;;
 */
static ins_8088_t Parse8088(memory_segment_t *feed, op_handler_t *handler){
    assert(feed->offset < feed->size);
    ins_8088_t ins;
    ins.meta.size = 0;

    memset(ins.opStr, 0, INS_STR_SZ);
    memset(ins.leftStr, 0, INS_STR_SZ);
    memset(ins.rightStr, 0, INS_STR_SZ);
    memset(&ins.dest, 0, sizeof(operand_t));
    memset(&ins.src, 0, sizeof(operand_t));

    int metaIdx = 0;

    bool found = false;
    int i = 0;

    for(; i < NUM_OP_CODES && !found; i++){
        if (MatchPrefix(feed->data[feed->offset],
                         opTable[i].prefixSize,
                         opTable[i].prefix)){
            if (opTable[i].extra == NO_MATCH){
                found = true;
                break;
            } else {
                uint8_t b = (feed->data[feed->offset + 1] & 0x38) >> 3;
                if (b == opTable[i].extra){
                    found = true;
                    break;
                }
            }
        }
    }

    if (!found){
        fprintf(stderr, "Cannot parse %d", feed->data[feed->offset]);
        feed->offset = feed->size;
        ins.meta.size = metaIdx;
        return ins;
    }

    uint8_t opcode    = 0;
    uint8_t direction = 0;
    uint8_t write     = 0;
    char *first = &ins.leftStr[0];
    char *second = &ins.rightStr[0];
    operand_t *srcPtr = &ins.src;
    operand_t *destPtr = &ins.dest;
    uint8_t byte1 = feed->data[feed->offset++];

    unpack_byte_t bits = UnpackByte(
        byte1, 
        3, 
        6, 1, 1);//OpCode Direction Write

    opcode    = bits.fields[0];
    direction = bits.fields[1];
    write     = bits.fields[2];
    SET_META_DATA(metaIdx, true, "opcode", opcode);
    SET_META_DATA(metaIdx, true, "direction", direction);
    SET_META_DATA(metaIdx, true, "write", write);

    ins.mnemonic = opTable[i].mnemonic;
    strncpy(ins.opStr, mnemonicLookup[opTable[i].mnemonic], OP_CODE_SZ);
    ins.parseMode = opTable[i].parseMode;
    SET_META_DATA(metaIdx, false, parse_mode_desc[opTable[i].parseMode], 0); 

    *handler = opTable[i].handler;

    //TODO can I use PackWord for each word value?
    switch(opTable[i].parseMode){

        case PM_NO_DIR_REG_MEM_W_REG:
        case PM_REG_MEM_W_REG:
        case PM_REG_MEM:
        case PM_REG_MEM_V:
        case PM_IMM_REG_MEM_NO_SIGN:
        case PM_IMM_REG_MEM: {
            uint8_t sign  = direction; //Can be V for logic

            mod_info_t modinfo = UnpackModInfo(feed);
            SetMetaModInfo(&metaIdx, &ins, modinfo);

            if (opTable[i].parseMode == PM_IMM_REG_MEM_NO_SIGN){
                sign = 0;
            }

            //Get the data
            if (opTable[i].parseMode == PM_REG_MEM ||opTable[i].parseMode == PM_REG_MEM_V){
                //TODO can we ever have byte here?
                char temp[100] = {0};
                GetModStr(modinfo, write, ins.leftStr, &ins.dest);
                strncpy(temp, ins.leftStr, INS_STR_SZ);
                sprintf(ins.leftStr, "%s %s", 
                    ((write) ? "word" : "byte"),
                    temp);

                ins.dest.isWord = write;

                //This is V in the manual for shifts
                if (opTable[i].parseMode == PM_REG_MEM_V){
                    if (sign){
                        sprintf(ins.rightStr, "cl");
                        ins.src.isWord = false;
                        ins.src.type = OT_REGISTER;
                        ins.src.reg = REG_CX;
                    } else {
                        sprintf(ins.rightStr,  "1");
                        ins.src.isWord = false;
                        ins.src.type = OT_IMMEDIATE;
                        ins.src.immediate = 1;
                    }
                }

           } else if(opTable[i].parseMode == PM_IMM_REG_MEM_NO_SIGN ||
                      opTable[i].parseMode == PM_IMM_REG_MEM) {

                GetModStr(modinfo, write, ins.leftStr, &ins.dest);

                if (!sign && write){
                    uint8_t data1 = feed->data[feed->offset++];
                    uint8_t data2 = feed->data[feed->offset++];
                    ins.src.isWord = true;
                    ins.src.type = OT_IMMEDIATE;
                    ins.src.immediate = PackWord(data1, data2);
                    SET_META_DATA(metaIdx, true, "16 bit data(low)", data1);
                    SET_META_DATA(metaIdx, true, "16 bit data(high)", data2);
                    sprintf(ins.rightStr, "word %d", ins.src.immediate);
                } else if (sign && write){
                    // The 8089 performs arithmetic operations to 20
                    // significant bits as follows. Byte and word
                    // operands are sign-extended to 20 bits (e.g., bit 7
                    // of a byte operand is propagated through bits 8-19
                    // of an internal register). Sign extension does not
                    // affect the magnitude of the operand.
                    SET_META_DATA(metaIdx, true, "8 bit data extended", feed->data[feed->offset]);
                    ins.src.isWord = true;
                    ins.src.type = OT_IMMEDIATE;
                    ins.src.immediate = feed->data[feed->offset++];
                    if (ins.src.immediate & 0x80){
                        ins.src.immediate |= 0xff00;
                    }
                    sprintf(ins.rightStr, "word %d", ins.src.immediate);
                } else {
                    SET_META_DATA(metaIdx, true, "8 bit data", feed->data[feed->offset]);
                    ins.src.isWord = false;
                    ins.src.type = OT_IMMEDIATE;
                    ins.src.immediate = feed->data[feed->offset++];
                    sprintf(ins.rightStr, "byte %d", ins.src.immediate);
                }

            } else {
                if (opTable[i].parseMode == PM_NO_DIR_REG_MEM_W_REG){
                    direction = 1;
                    write = 1;                                
                }

                if (!direction){
                    first = &ins.rightStr[0];
                    second = &ins.leftStr[0];
                    srcPtr = &ins.dest;
                    destPtr = &ins.src;
                }

                GetRegStr(modinfo.reg, write, first, destPtr);
                GetModStr(modinfo, write, second, srcPtr);
            }
        }
        break;

        case PM_IMM_REG:
        case PM_IMM_ACC_DIR:
        case PM_IMM_ACC: {
            uint8_t reg = 0;
            
            if (opTable[i].parseMode == PM_IMM_ACC || opTable[i].parseMode == PM_IMM_ACC_DIR){
                if (opTable[i].parseMode == PM_IMM_ACC_DIR && direction){
                    first = &ins.rightStr[0];
                    second = &ins.leftStr[0];
                    srcPtr = &ins.dest;
                    destPtr = &ins.src;
                }

                destPtr->type = OT_REGISTER;
                if (write){
                    destPtr->isWord = true;
                    sprintf(first, "ax");
                    destPtr->reg = REG_AX;
                } else {
                    destPtr->isWord = false;
                    sprintf(first, "al");
                    destPtr->reg = REG_AL;
                }
            } else {
                 unpack_byte_t bits = UnpackByte(
                            byte1, 
                            3, 
                            4, 1, 3);//OpCode Write

                opcode = bits.fields[0];
                write = bits.fields[1];
                reg = bits.fields[2];

                GetRegStr(reg, write, ins.leftStr, &ins.dest);
            }

            SET_META_DATA(metaIdx, true, "write", write);

            int16_t data1 = feed->data[feed->offset++];
            SET_META_DATA(metaIdx, true, "data 1", data1);


            if (write || opTable[i].parseMode == PM_IMM_ACC_DIR){
                int16_t data2 = feed->data[feed->offset++];
                int16_t word = ((data2 << 8) & 0xff00) | (data1 & 0xff);
                SET_META_DATA(metaIdx, true, "data 1", data1);
                SET_META_DATA(metaIdx, true, "data 2", data2);

                if (opTable[i].parseMode == PM_IMM_ACC_DIR){
                    sprintf(second, "[%d]", word);
                    srcPtr->type = OT_ADDRESS;
                    srcPtr->address = word;
                } else {
                    sprintf(second, "%d", word);
                    srcPtr->type = OT_IMMEDIATE;
                    srcPtr->immediate = word;
                }
            } else {
                srcPtr->immediate = data1;
                srcPtr->type = OT_IMMEDIATE;
                sprintf(second, "%d", data1);
            }
        }
        break;

        case PM_LABEL:
        case PM_TYPE_SPEC: {
            if (write || opTable[i].parseMode == PM_LABEL){
                int8_t data = feed->data[feed->offset++];
                SET_META_DATA(metaIdx, true, "data 1", data);
                sprintf(ins.leftStr, "%d", data);
                ins.dest.immediate = data;
                ins.dest.type = OT_IMMEDIATE;
            } else {
                //TODO I'm not sure how to represent this as an operand
                strncat(ins.opStr, "3", INS_STR_SZ);
            }
        }
        break;

        case PM_ONE_WORD: {
            int8_t hasWord = !write;

            if (hasWord){
                int16_t data1 = feed->data[feed->offset++];
                int16_t data2 = feed->data[feed->offset++];
                int16_t word = ((data2 << 8) & 0xff00) | (data1 & 0xff);
                SET_META_DATA(metaIdx, true, "data1", data1);
                SET_META_DATA(metaIdx, true, "data 2", data2);
                sprintf(ins.leftStr, "%d", word);
                ins.dest.immediate = word;
                ins.dest.type = OT_IMMEDIATE;
            }
        }
        break;

        case PM_REG_W_ACC:
        case PM_REG: {
            uint8_t reg = ((opcode & 1) << 2) | (direction << 1) | write;

            if (opTable[i].parseMode == PM_REG){
                GetRegStr(reg, 1, ins.leftStr, &ins.dest);
            } else {
                strncpy(ins.leftStr, "ax", INS_STR_SZ);

                ins.dest.type = OT_REGISTER;
                ins.dest.reg = REG_AX;
                GetRegStr(reg, 1, ins.rightStr, &ins.src);
            }

            SET_META_DATA(metaIdx, true, "register", reg);
         }
        break;

        case PM_SEG_REG: {
            unpack_byte_t bits = UnpackByte(
                byte1, 
                3, 
                3, 2, 3);//OpCode Write

            uint8_t part1 = bits.fields[0];
            uint8_t reg = bits.fields[1];
            uint8_t part2 = bits.fields[2];
            SET_META_DATA(metaIdx, true, "register", reg);
            SET_META_DATA(metaIdx, true, "part 2", part2);

            GetSegRegStr(reg, ins.leftStr, &ins.dest);
        }
        break;

        case PM_VAR_PORT:
        case PM_FIXED_PORT: {
            if (direction){
                first = &ins.rightStr[0];
                second = &ins.leftStr[0];
                destPtr = &ins.src;
                srcPtr = &ins.dest;
            }

            if (write){
                sprintf(first, "ax");
                destPtr->reg = REG_AX;
                destPtr->type = OT_REGISTER;
            } else {
                sprintf(first, "al");
                destPtr->reg = REG_AL;
                destPtr->type = OT_REGISTER;
            }

            if (opTable[i].parseMode == PM_FIXED_PORT){
                uint8_t data1 = feed->data[feed->offset++];
                SET_META_DATA(metaIdx, true, "data 1", data1);
                sprintf(second, "%d", data1);
                srcPtr->immediate = data1;
                srcPtr->type = OT_IMMEDIATE;
            } else {
                sprintf(second, "dx");
                srcPtr->reg = REG_DX;
                srcPtr->type = OT_REGISTER;
            }

        }
        break;

        case PM_REP: {
            uint8_t z = write; 
            if (z == 0){
                //TODO I'm not sure how to represent this as an operand
                strncat(ins.opStr, "nz", INS_STR_SZ);
            }
        }
        break;

        case PM_DOUBLE:
        case PM_SINGLE: {
            //TODO what do I store in the operand in this case?
            if (opTable[i].parseMode == PM_DOUBLE){
                feed->offset++;
            }
        }
        break;

        case PM_DIRECT_IN_SEG_SHORT: {
            int8_t low = feed->data[feed->offset++];

            SET_META_DATA(metaIdx, true, "low", low);
            sprintf(ins.leftStr, "%d", low);
            ins.dest.immediate = low;
            ins.dest.type = OT_IMMEDIATE;
        }
        break;
        case PM_DIRECT_IN_SEG: {
            uint8_t low    = feed->data[feed->offset++];
            uint8_t high   = feed->data[feed->offset++];
            uint16_t inc   = PackWord(low, high);

            SET_META_DATA(metaIdx, true, "low", low);
            SET_META_DATA(metaIdx, true, "high", high);
            SET_META_DATA(metaIdx, true, "inc", inc);

            sprintf(ins.leftStr, "%d", inc);
            ins.dest.immediate = inc;
            ins.dest.type = OT_IMMEDIATE;
        }
        break;

        case PM_PREFIX: {
            //TODO I'm not crazy about doing string comparisons here
            if (direction){
                srcPtr = &ins.dest;
                destPtr = &ins.src;
            }

            srcPtr->type = OT_NONE;
            destPtr->type = OT_NONE;

            if (opTable[i].mnemonic == OP_ES){
                srcPtr->type = OT_SEG_REG;
                srcPtr->segreg = REG_ES;
            } else if (opTable[i].mnemonic == OP_CS){
                srcPtr->type = OT_SEG_REG;
                srcPtr->segreg = REG_CS;
            } else if (opTable[i].mnemonic == OP_SS){
                srcPtr->type = OT_SEG_REG;
                srcPtr->segreg = REG_SS;
            } else if (opTable[i].mnemonic == OP_DS){
                srcPtr->type = OT_SEG_REG;
                srcPtr->segreg = REG_DS;
            }

            //TODO handle lock and assert on anything else
 
        }
        break;

        case PM_INDIRECT_INTERSEGMENT: {
            mod_info_t modinfo = UnpackModInfo(feed);
            SetMetaModInfo(&metaIdx, &ins, modinfo);
            char temp[100] = {0};
            //TODO can this just take a modinfo?
            GetModStr(modinfo, write, ins.leftStr, &ins.dest);
            strncpy(temp, ins.leftStr, INS_STR_SZ);
            sprintf(ins.leftStr, "%s %s", 
                ("far"),
                temp);

        }
        break;
    
        case PM_DIRECT_INTERSEGMENT: {
            int16_t ipLow = feed->data[feed->offset++];
            int16_t ipHigh = feed->data[feed->offset++];
            int16_t ipWord = ((ipHigh << 8) & 0xff00) | (ipLow & 0xff);
            int16_t csLow = feed->data[feed->offset++];
            int16_t csHigh = feed->data[feed->offset++];
            int16_t csWord = ((csHigh << 8) & 0xff00) | (csLow & 0xff);
            //TODO come back to this one to encode the operand
            sprintf(ins.leftStr, "%d:%d", csWord, ipWord);
        }
        break;

        //case PM_SEG_REG_REG_MEM: {
        //}

        case PM_REG_MEM_SEG_REG: {
            mod_info_t modinfo = UnpackModInfo(feed);
            SetMetaModInfo(&metaIdx, &ins, modinfo);
            if (direction){
                first = &ins.rightStr[0];
                second = &ins.leftStr[0];
                srcPtr = &ins.dest;
                destPtr = &ins.src;
            }

            GetModStr(modinfo, 1, first, destPtr);
            GetSegRegStr(modinfo.reg, second, srcPtr);
        }
        break;

        case PM_STRING_MAN: {
            //TODO yeah, not sure how encode this one
            strncat(ins.opStr, (write) ? "w": "b", INS_STR_SZ);
        }
        break;
    }

    ins.meta.size = metaIdx;
    return ins;
}

static void ToBinaryString(uint8_t x, char *buffer){
    int n = 0;
    for(int i = 0; i < 8; i++){
        if(i == 4){
            buffer[n++] = ' ';
        }
        buffer[n++] = (0x80 & x) > 0 ? '1' : '0';
        x = x << 1;
    }
    buffer[n] = '\0';
}

static void ToHexString(uint8_t x, char *buffer){
    sprintf(buffer, "%.2x", x);
}
