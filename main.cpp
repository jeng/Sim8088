// Jeremy English jhe@jeremyenglish.org
//
// This file is a heavily modified version of https://github.com/ocornut/imgui
//
// DONE Add a stack view
// TODO Add a display for recently changed memory locations
//      - This will be similar to the stack view but only keep a set history
// DONE Add configuration for framebuffer location
//      - This should take the segment
//      - This should take an offset in the segment
// TODO Add a raw memory view
//      - It would be nice to jump to the change location
//      - Also highlight this location
// TODO Add a flow chart like view
//      - This would give me a chance to deep dive on something like
//        this Knuth paper https://dl.acm.org/doi/pdf/10.1145/367593.367620
// TODO Put all of the windows under a primary menu
// TODO Add test file runner
//      - Contains an initial register and flag setup
//      - Contains a hexdump of the program (or a location)
//      - Contains an expected state of registers and flags after execution
//      - Contains test name

// don't listen to MS complains, we want cross-platform code
#define _CRT_SECURE_NO_DEPRECATE

// C++
#include <iostream>
#include <sstream>

// SDL
#include <glad/glad.h>
#include <SDL.h>

// Dear ImGui
// #include "imgui.h"
#include "imgui-style.h"
#include "imgui/imgui_impl_sdl.h"
#include "imgui/imgui_impl_opengl3.h"

//Sim86 specific headers
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <stdint.h>
#include <limits.h>

#include "sim8088.h"
#include "sim8088_machine.cpp"
#include "sim8088.cpp"
#include "sim8088_cycles.cpp"

#define IM_MIN(A, B)            (((A) < (B)) ? (A) : (B))
#define IM_MAX(A, B)            (((A) >= (B)) ? (A) : (B))
#define IM_CLAMP(V, MN, MX)     ((V) < (MN) ? (MN) : (V) > (MX) ? (MX) : (V))

int windowWidth = 1280,
    windowHeight = 720;

#define MAX_BUF_SZ 4096
#define MAX_ASM_LINES 13000
#define RED_ZONE_SZ 100
#define EVAL_ON 1
#define MAX_BREAKS 1024
#define MAX_PARSE_TABLE 65536

struct breakpoints_t {
    int size;
    int count;
    uint16_t breaks[MAX_BREAKS];
};

struct parse_table_entry_t {
    ins_8088_t ins;
    op_handler_t handler;
    uint16_t length;
};

struct framebuffer_settings_t {
    bool showing;
    uint8_t segreg;
    uint16_t offset;
};
  
struct environment_t {
    memory_segment_t data;
    bool hexDisplay;
    char filename[MAX_BUF_SZ];
    char savefile[MAX_BUF_SZ];
    bool lineFlags[MAX_ASM_LINES];
    char message[MAX_BUF_SZ];
    char dumpfile[MAX_BUF_SZ];
    sim86_machine_t machine;
    breakpoints_t breakpoints;
    bool dumpMemory[4];//Which segments are we dumping?
    GLuint texture;//Texture used for the framebuffer
    parse_table_entry_t *parseTable;
    framebuffer_settings_t framebufferSettings;
    uint32_t totalCycles;
};

void AddBreakpoint(breakpoints_t *breakpoints, uint16_t address){
    //TODO better error handling
    assert(breakpoints->count < breakpoints->size);
    breakpoints->breaks[breakpoints->count++] = address;
}

void ClearBreakpoints(breakpoints_t *breakpoints){
    breakpoints->count =0;
    memset(breakpoints->breaks, 0, MAX_BREAKS);
}

bool IsBreakpoint(breakpoints_t *breakpoints, uint16_t address){
    for(int i =0; i < breakpoints->count; i++){
        if(breakpoints->breaks[i] == address){
            return true;
        }
    }
    return false;
}

//TODO do not print to stderr here.  Return an error result
//TODO set a max size based on the segment being loaded
uint8_t* Slurp(char *filename, uint8_t *buffer, uint16_t *resultSize){
    FILE *file;
    *resultSize = 0;
    file = fopen(filename, "rb");
    if (file == NULL){
        fprintf(stderr, "Could not open file.\n");
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    size_t fileSize = ftell(file);
    rewind(file);
    if (buffer){
        size_t result = fread(buffer, sizeof(char), fileSize, file);
        if (result != fileSize){
            fprintf(stderr, "Could not read all bytes");
            return NULL;
        }   
        
        //Casting here so that I don't have to worry about casting size_t
        //everywhere else

        *resultSize = (uint16_t)result;
    } else {
        fprintf(stderr, "Could not allocate buffer");
        return NULL;
    }
    return buffer;
}

void ShowCodeSegmentBinaryOutput(environment_t *env){
    char s[10];
    int n = 0;
    int parseOffset = 0;
    int parseCount = -1;
    //op_handler_t handler;

    memory_segment_t data;
    data.data = &env->machine.memory[env->machine.segregs[REG_CS] << 4];
    data.size = env->data.size;
    data.offset = 0;//TODO might be more to it than this
    data.startAddress = 0;

    ImGui::BeginChild("Scrolling");
    
    ImGui::TextColored(ImVec4(0.463f,0.925f,0.988f,1.0f), 
            "%s:%04x", segregs[REG_CS], data.startAddress);
    
    int newline = (env->hexDisplay) ? 16 : 8;

    for(int i = 0; i < data.size; i++){
        //ins_8088_t ins;
        parse_table_entry_t entry;

        if (parseOffset == data.offset || parseCount == -1){
            entry = env->parseTable[data.offset];
            data.offset += entry.length;
            //ins = Parse8088(&data, &handler);
            parseCount++;
            parseOffset++;
        } else {
            parseOffset++;
        }

        // std::cerr << "parse offset " 
        //           << parseOffset << " parse count " 
        //           << parseCount << std::endl;

        if (n == newline){
            ImGui::TextColored(ImVec4(0.463f,0.925f,0.988f,1.0f), 
                    "%s:%04x", segregs[REG_CS], data.startAddress + ((i+newline) - n));
            n = 0;
        }

        if (env->hexDisplay){
            ToHexString(data.data[i], s);
        } else {
            ToBinaryString(data.data[i], s);
        }

        if (parseCount < 0 || MAX_ASM_LINES <= parseCount){
            sprintf(env->message, "parse count out of range %d", parseCount);
            break;
        } 


        if (env->lineFlags[parseCount]){
            ImGui::SameLine(); ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%s ", s);
        } else {
            ImGui::SameLine(); ImGui::Text("%s ", s);
        }
    
        n++;
    }

    ImGui::EndChild();
}

void ShowBinaryOutput(environment_t *env, seg_reg_t segreg, uint16_t size){
    char s[10];
    int n = 0;

    memory_segment_t data;
    data.data = &env->machine.memory[env->machine.segregs[segreg] << 4];
    data.size = size;
    data.offset = 0;//TODO might be more to it than this
    data.startAddress = 0;

    ImVec2 child_size = ImVec2(0, ImGui::GetFontSize() * 20.0f);
    ImGui::BeginChild("##ScrollingRegion", child_size, false, ImGuiWindowFlags_HorizontalScrollbar);
    
    int newline = 16;
    int numLines = (size/newline) + 1;

    ImGuiListClipper clipper;
    clipper.Begin(numLines);
    while (clipper.Step()){
        n = newline;
        for(int i = (clipper.DisplayStart * newline); i < IM_MIN(size, clipper.DisplayEnd * newline); i++){
            //ins_8088_t ins;

            if (n == newline){
                ImGui::TextColored(ImVec4(0.463f,0.925f,0.988f,1.0f), 
                        "%s:%04x", segregs[segreg], data.startAddress + ((i+newline) - n));
                n = 0;
            }

            ToHexString(data.data[i], s);
            ImGui::SameLine(); ImGui::Text("%s ", s);
        
            n++;
        }
    }
    clipper.End();
    ImGui::EndChild();

}

void ShowDisassembleOutput(environment_t *env){
    int n = 0;
    memory_segment_t *data = &env->data;
    env->totalCycles = 0;
    //op_handler_t handler;
    ImGui::BeginChild("Scrolling");
    for(data->offset = 0; data->offset < data->size; ){
        int currentOffset = data->offset;
        //ins_8088_t ins = Parse8088(data, &handler);
        parse_table_entry_t entry = env->parseTable[data->offset];
        data->offset += entry.length;
        ins_8088_t *ins = &entry.ins;

        //4 because of the space and null char
        char s[(INS_STR_SZ * 3) + 4] = {0}; 
        sprintf(s, "%s %s%s %s", 
            ins->opStr,
            ins->leftStr,
            ((strlen(ins->rightStr) > 0) ? ",": ""),
            ins->rightStr);


        if (n < MAX_ASM_LINES){
            if (env->machine.pc <= currentOffset && currentOffset <= env->machine.pc){
                ImGui::TextColored(ImVec4(0.988f,0.925f,0.463f,1.0f), "CS:%.4x", currentOffset);
            } else {
                ImGui::TextColored(ImVec4(0.463f,0.925f,0.988f,1.0f), "CS:%.4x", currentOffset);
            }

            ImGui::SameLine();
            ImGui::Selectable(s, &env->lineFlags[n], 0, ImVec2(ImGui::GetFontSize() * 25, 0));
            ImGui::SameLine();
            uint16_t cycles = GetCycleCount(ins);
            env->totalCycles += cycles;

            ImGui::TextColored(ImVec4(0.75f, 0.66f, 0.66f, 1.0f), "%4d", GetCycleCount(ins));

            if (env->lineFlags[n]){
                AddBreakpoint(&env->breakpoints, currentOffset);
            }
        } else {
            ImGui::Text("out of lines");
            data->offset = data->size; // exit
        }

        if (n < 0 || MAX_ASM_LINES <= n){
            sprintf(env->message, "parse count out of range %d", n);
            break;
        } 

        if (env->lineFlags[n]){
            for(uint32_t i = 0; i < ins->meta.size; i++){
                char s[10];
                ToBinaryString((uint8_t)ins->meta.entries[i].value, s);
                ImGui::Text("%s %d (%s)", 
                    ins->meta.entries[i].desc,
                    ins->meta.entries[i].value, s);
            }
        }

        n++;
    }
    ImGui::EndChild();
}

int GetLabelLocation(environment_t *env, ins_8088_t *ins){
    int lblOffset = atoi(ins->leftStr);
    size_t location = env->data.offset + lblOffset;
    return (int)location; //TODO it would be crazy if someone is using this tool for > 2gb file
}

int LabelExist(int *labels, int size, int location){
    for(int i = 0; i < size && i < MAX_LBL; i++){
        if (labels[i] == location){
            return i;
        }
    }
    return -1;
}

int GetLabels(environment_t * env, int *labels){
    //First pass get all the labels
    //Second pass patch label instructions

    op_handler_t handler;
    int lblIdx = 0;
    for(env->data.offset = 0; env->data.offset < env->data.size; ){
        ins_8088_t ins = Parse8088(&env->data, &handler);
        if (ins.parseMode == PM_LABEL ||
            ins.parseMode == PM_DIRECT_IN_SEG || 
            ins.parseMode == PM_DIRECT_IN_SEG_SHORT){

            int location = GetLabelLocation(env, &ins);
            if (LabelExist(labels, lblIdx, location) == -1){
                labels[lblIdx++] = location;
                assert(lblIdx < MAX_LBL);
            }
        }
    }
    return lblIdx;
}

void WriteDisassembleFile(environment_t *env){
    FILE *output = fopen(env->savefile, "w");
    if (!output){
        fprintf(stderr, "Could not open file\n");
        return;
    }

    int labels[MAX_LBL] = {0}; 
    int numLbls = GetLabels(env, labels);

    memory_segment_t *data = &env->data;
    op_handler_t handler;

    const char *b = "bits 16\n";
    fwrite(b, sizeof(char), strnlen(b, 10), output);

    for(data->offset = 0; data->offset < data->size; ){
        ins_8088_t ins = Parse8088(data, &handler);
        char s[1024] = {0};

        //kludge
        if (ins.parseMode == PM_LABEL ||
            ins.parseMode == PM_DIRECT_IN_SEG ||
            ins.parseMode == PM_DIRECT_IN_SEG_SHORT){

            int location = GetLabelLocation(env, &ins);
            int n = LabelExist(labels, numLbls, location);
            if (n == -1){
                sprintf(s, "Cound not find label %d\n", (int)data->offset);
                fwrite(s, sizeof(char), strnlen(s, 1024), output);
                break;
            }
            ins.leftStr[0] = '\0';
            sprintf(ins.leftStr, "lbl%.3d", n);
        }

        sprintf(s, "%s %s%s %s\n", 
            ins.opStr, 
            ins.leftStr, 
            ((strnlen(ins.rightStr, INS_STR_SZ) > 0) ? ",": ""), 
            ins.rightStr);

        fwrite(s, sizeof(char), strnlen(s, INS_STR_SZ), output);

        //and more kludge
        int lblCurrent = LabelExist(labels, numLbls, (int)data->offset);
        if (lblCurrent != -1){
            char slbl[10] = {0};
            sprintf(slbl, "lbl%.3d:\n", lblCurrent);
            fwrite(slbl, sizeof(char), strlen(slbl), output);
        }
    }
    fclose(output);
}

// I would like to parse the file now and store it in memory.  Our code segment
// is limited 65536 bytes.  We will have less instructions than that unless a
// program was loaded with only one byte instructions.
// 
// More memory will be need to store the dissembled strings.  The table will
// have fragmentation since we will want to have O(1) lookup based off of the
// current offset.
// 
// This will break self modifying code but we can act like the code segment is
// in ROM and couldn't be modified anyway.  Plus I can set the data segment a
// good distance from the code segment to avoid collision.

void LoadParsedProgram(environment_t *env){
    op_handler_t handler;
    for(env->data.offset = 0; env->data.offset < env->data.size; ){
        //TODO Why is offset size_t all of the segments have max 0xffff bytes
        int currentOffset = env->data.offset;
        ins_8088_t ins = Parse8088(&env->data, &handler);

        env->parseTable[currentOffset].ins = ins;
        env->parseTable[currentOffset].handler = handler;
        env->parseTable[currentOffset].length = env->data.offset - currentOffset;
    }
}

void LoadCodeSegment(environment_t *env, bool *loaded){
    if (!*loaded){
        //TODO do we want to reset the code segment register on load?
        env->data.data = &env->machine.memory[env->machine.segregs[REG_CS] << 4];
        Slurp(env->filename, env->data.data, &(env->data.size));
        env->data.offset = 0;
        LoadParsedProgram(env); 
        *loaded = true;
    }                
}

void OpenCodeSegmentDisplayWindow(environment_t *env){
    ImGui::SetNextWindowSize(
        ImVec2(400.0f, 350.0f),
        ImGuiCond_FirstUseEver);

    ImGui::Begin("Code Segment (tracking)", NULL);

    ImVec2 windowSize = ImGui::GetWindowSize();
    ImGui::PushItemWidth(windowSize.x - 15);
    ImGui::Text("%s", env->filename);

    ShowCodeSegmentBinaryOutput(env);

    ImGui::End();
}


void OpenBinaryDisplayWindow(environment_t *env, seg_reg_t segreg, uint16_t size){
    ImGui::SetNextWindowSize(
        ImVec2(400.0f, 350.0f),
        ImGuiCond_FirstUseEver);

    ImGui::Begin(segregsDesc[segreg], NULL, ImGuiWindowFlags_MenuBar);

    //ImVec2 windowSize = ImGui::GetWindowSize();
    //ImGui::PushItemWidth(windowSize.x + 15);
    if (ImGui::BeginMenuBar()){
        if (ImGui::BeginMenu("Menu")){
            ImGui::MenuItem("Dump to file", NULL, &env->dumpMemory[segreg]);
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }

    ShowBinaryOutput(env, segreg, size);

    ImGui::End();
}

void OpenDisassembleDisplayWindow(environment_t *env){
    ImGui::SetNextWindowSize(
        ImVec2(400.0f, 350.0f),
        ImGuiCond_FirstUseEver);

    ImGui::Begin("Disassemble", NULL);

    ImVec2 windowSize = ImGui::GetWindowSize();
    ImGui::PushItemWidth(windowSize.x - 15);
    ShowDisassembleOutput(env);

    ImGui::End();
}

void DumpOpTable(environment_t *env){
    FILE *output = fopen(env->dumpfile, "w");
    if (!output){
        fprintf(stderr, "Could not open file\n");
        return;
    }

    for(int i = 0; i < NUM_OP_CODES; i++){
        char s[1024] = {0};
        char sb1[10] = {0};
        char sb2[10] = {0};
        char senum[100] = {0};
        int shift = 8 - opTable[i].prefixSize;
        int16_t prefix = opTable[i].prefix;
        //TODO check for the negative prefixes
        prefix <<= shift;
        ToBinaryString((uint8_t)prefix, sb1);
        if (opTable[i].extra != NO_MATCH){
            ToBinaryString((char)opTable[i].extra, sb2);
        }
#if 1
        sprintf(s, "%s\t%s\t%s\t%s\n", mnemonicLookup[opTable[i].mnemonic], sb1, sb2, parse_mode_desc[opTable[i].parseMode]);
#else
        char match2str[1024] = {0};
        if (opTable[i].pattern2Size == 3){
            sprintf(match2str, "0x%.2x", opTable[i].match2[1]);
        } else {
            sprintf(match2str, "NO_MATCH");
        }
        sprintf(s, "    {\"%s\", %d, 0x%.2x, %s, %s},\n", 
            opTable[i].opCode, 
            opTable[i].pattern1[0], 
            opTable[i].match1[0],
            match2str,
            parse_mode_desc[opTable[i].parseMode]);
#endif
            
        fwrite(s, sizeof(char), strnlen(s, 1024), output);
    }

    fclose(output);
}


void DumpMemory(environment_t *env, char *filename, uint32_t startAddress, uint16_t numBytes){
    FILE *output = fopen(filename, "wb");

    if ((startAddress + numBytes) > MAX_MEM){
        fprintf(stderr, "Trying to write past the end of memory\n");
        return;
    }

    if (!output){
        fprintf(stderr, "Could not open file\n");
        return;
    }

    fwrite(&env->machine.memory[startAddress], sizeof(char), numBytes, output);

    fclose(output);
}

uint32_t GetFramebufferPhysicalAddress(environment_t *env){
    uint8_t segreg = env->framebufferSettings.segreg;
    uint16_t offset = env->framebufferSettings.offset;
    return (env->machine.segregs[segreg] << 4) + offset;
}

void InitFramebuffer(environment_t *env, uint8_t *outWidth, uint8_t *outHeight){
    //TODO pass in the start address
    uint8_t imageWidth = 64;
    uint8_t imageHeight = 64;
    uint8_t *imageData = &env->machine.memory[GetFramebufferPhysicalAddress(env)];
    
    // Create a OpenGL texture identifier
    GLuint imageTexture;
    glGenTextures(1, &imageTexture);
    glBindTexture(GL_TEXTURE_2D, imageTexture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // Same

    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

    env->texture = imageTexture;
    *outWidth    = imageWidth;
    *outHeight   = imageHeight;
}

void UpdateFramebuffer(environment_t *env){
    uint8_t imageWidth = 64;
    uint8_t imageHeight = 64;
    uint8_t *imageData = &env->machine.memory[GetFramebufferPhysicalAddress(env)];

    glBindTexture(GL_TEXTURE_2D, env->texture);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, imageWidth, imageHeight, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
}

void OpenFramebuffer(environment_t *env, uint8_t width, uint8_t height){
    GLuint *texture = &env->texture;

    ImGui::Begin("Framebuffer", NULL, ImGuiWindowFlags_MenuBar);

    if (ImGui::BeginMenuBar()){
        if (ImGui::BeginMenu("Menu")){
            ImGui::MenuItem("Settings", NULL, &env->framebufferSettings.showing);
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }

    //ImGui::Text("pointer = %p", *texture);
    //ImGui::Text("size = %d x %d", width, height);
    ImGui::Image((void*)(intptr_t)*texture, ImVec2((float)width * 4, (float)height * 4));
    ImGui::End();
}

void OpenStackView(environment_t *env){
    uint8_t segreg = REG_SS;
    uint32_t offset = env->machine.registers[REG_SP];
    uint32_t physicalAddress = (env->machine.segregs[segreg] << 4) + offset;
    uint32_t n = 0;

    ImGui::Begin("Stack View");
    ImGui::BeginChild("Stack Lines");
    for(int i = offset; i <0xffff; i+=2){
        char line[10];
        uint8_t low = env->machine.memory[physicalAddress + n];
        uint8_t high = env->machine.memory[physicalAddress + n + 1];
        uint16_t value = (high << 8) | low;
        n+=2;
        sprintf(line, "%.4x", value);
        ImGui::Text("%s ", line);
    }
    ImGui::EndChild();
    ImGui::End();
}

void StepMachine(environment_t *env, bool *blitOccured){
    memory_segment_t *data = &env->data;
    //op_handler_t handler;

    //TODO kind of a hack, we might want to update the PC in each op
    uint16_t currentPC = env->machine.pc;
    data->offset = env->machine.pc;
    if (data->offset < data->size){
        parse_table_entry_t entry = env->parseTable[data->offset];
        data->offset += entry.length;
        entry.ins.length = entry.length; //TODO fix this to only have one length
        //ins_8088_t ins = Parse8088(data, &handler);
        if (entry.handler){
            entry.handler(&env->machine, &entry.ins);
        }
    }            

    if (env->machine.blit){
        UpdateFramebuffer(env);
        env->machine.blit = false;
        *blitOccured = true;
    }

    env->machine.pc += (data->offset - currentPC);

    //hack to actually stop single stepping
    if (env->machine.halt){
        env->machine.pc = data->size;
    }
}


void ExecuteMachine(environment_t *env, bool *execute, int maxSteps){
    memory_segment_t *data = &env->data;
    int n = 0;
    env->machine.halt = false;
    for(data->offset = env->machine.pc; data->offset < data->size;){
        bool blitOccured = false;
        StepMachine(env, &blitOccured);

        if (blitOccured)
            return;

        if (IsBreakpoint(&env->breakpoints, env->machine.pc)){
            *execute = false;
            return;
        }

        data->offset = env->machine.pc;
        if (n++ >= maxSteps){
            return;
        }

        if (env->machine.halt){
            return;
        }
    }
}

//TODO do I need to pass in an isOpen flag?
void OpenFramebufferSettings(environment_t *env) {
    //
    //TODO do these need to be static?
    static char segmentOffset[10] = {0};
    static char message[1024] = {0};
    static uint16_t offset = env->framebufferSettings.offset;

    ImGui::Begin("Framebuffer Settings");
    ImGuiComboFlags flags = 0;
    int currentIdx = env->framebufferSettings.segreg;
    if (ImGui::BeginCombo("Segment", segregsDesc[currentIdx], flags)){
        for(int i = 0; i < NUM_SEG_REGS; i++){
            bool isSelected = (env->framebufferSettings.segreg == i);

            if(ImGui::Selectable(segregsDesc[i], isSelected))
                env->framebufferSettings.segreg = i;

            if (isSelected)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }

    ImGui::SameLine();

    sprintf(segmentOffset, "%d", offset);
    if(ImGui::InputText("Offset", segmentOffset, 10)){
        offset = atoi(segmentOffset);
        if (0 <= offset && offset <= 0xffff){
            env->framebufferSettings.offset = offset;
        }
    }

    if (ImGui::Button("Close")){
        if (0 <= offset && offset <= 0xffff){
            env->framebufferSettings.showing = false;
            UpdateFramebuffer(env);
        } else {
            sprintf(message, "Invalid segment offset");
            ImGui::End();
            return;
        }
    }

    if (strlen(message) > 0){
        ImGui::TextUnformatted(message);
    }


    ImGui::End();
}


void DumpMemoryToFile(environment_t *env, bool* isOpen, seg_reg_t segreg) {
    if (!ImGui::Begin("Memory Dump", isOpen, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::End();
        return;
    }

    static int lines = 10;
    static char filename[MAX_BUF_SZ] = {0};
    static char startLocation[10] = {0};
    static char numberOfBytes[10] = {0};
    static char message[1024] = {0};
    ImGui::TextUnformatted(segregsDesc[segreg]);
    ImGui::InputText("Starting Location", startLocation, 10);
    ImGui::InputText("Number of Bytes", numberOfBytes, 10);
    ImGui::InputText("Save File", filename, MAX_BUF_SZ);

    if (ImGui::Button("Save")){
        message[0] = 0;
        uint32_t start = atoi(startLocation);
        if (start > 0xffff){
            sprintf(message, "Start location too large");
            ImGui::End();
            return;
        }

        uint32_t numBytes = atoi(numberOfBytes);
        if (numBytes == 0){
            sprintf(message, "Number of bytes needed");
            ImGui::End();
            return;
        }

        if (start + numBytes > 0xffff){
            sprintf(message, "Number of bytes is too large");
            ImGui::End();
            return;
        }

        start = (env->machine.segregs[segreg] << 4) + start;
        DumpMemory(env, filename, start, numBytes);
        *isOpen = false;
    }

    if (strlen(message) > 0){
        ImGui::TextUnformatted(message);
    }

    ImGui::End();
}

void Reset(environment_t *env){

    env->machine.segregs[REG_CS] = 0x39dd;
    env->machine.segregs[REG_DS] = 0x39dd;
    env->machine.segregs[REG_SS] = 0x39dd;
    env->machine.registers[REG_SP] = 0xFFFC;
    env->data.startAddress = 0;
    env->breakpoints.size = MAX_BREAKS;
    ClearBreakpoints(&env->breakpoints);
    memset(env->lineFlags, 0, MAX_ASM_LINES);
    env->data.data = NULL;
    env->data.size = 0;
    env->data.offset = 0;
    env->message[0] = '\0';
    env->totalCycles = 0;

}


int main(int argc, char *argv[])
{
    environment_t env;
    memset(env.lineFlags, 0, MAX_ASM_LINES);
    memset(env.savefile,  0, MAX_BUF_SZ);
    memset(env.filename,  0, MAX_BUF_SZ);
    memset(env.message,  0, MAX_BUF_SZ);
    memset(env.dumpfile,  0, MAX_BUF_SZ);

    env.data.data = NULL;
    env.data.size = 0;
    env.data.offset = 0;
    env.hexDisplay = false;
    env.machine = BuildMachine();

    env.parseTable = (parse_table_entry_t*)calloc(MAX_PARSE_TABLE, sizeof(parse_table_entry_t));

    env.dumpMemory[REG_ES] = false;
    env.dumpMemory[REG_CS] = false;
    env.dumpMemory[REG_DS] = false;
    env.dumpMemory[REG_SS] = false;

    //Keep the frame buffer settings during a reset by having them here instead
    //of in the reset procedure
    env.framebufferSettings.showing = false;
    env.framebufferSettings.segreg = REG_DS;
    env.framebufferSettings.offset = 0x100;

    Reset(&env);

    bool initFramebuffer = true;
    uint8_t framebufferWidth = 0;
    uint8_t framebufferHeight = 0;
    bool executeMachine = false;
    int maxSteps = 64 * 64;

    //TODO Move all of this initialization code somewhere else

    // initiate SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
        printf("[ERROR] %s\n", SDL_GetError());
        return -1;
    }

    SDL_version compiled;
    SDL_VERSION(&compiled);
    std::ostringstream compiledVal;
    compiledVal << "Compiled with "
                << std::to_string(compiled.major)
                << "." << std::to_string(compiled.minor)
                << "." << std::to_string(compiled.patch);

    SDL_version linked;
    SDL_GetVersion(&linked);
    std::ostringstream linkedVal;
    linkedVal << "Linked with "
              << std::to_string(linked.major)
              << "." << std::to_string(linked.minor)
              << "." << std::to_string(linked.patch);

    // setup SDL window

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    SDL_GL_SetAttribute(
        SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE
        );

    std::string glsl_version = "";
#ifdef __APPLE__
    // GL 3.2 Core + GLSL 150
    glsl_version = "#version 150";
    SDL_GL_SetAttribute( // required on Mac OS
        SDL_GL_CONTEXT_FLAGS,
        SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG
        );
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
#elif __linux__
    // GL 3.2 Core + GLSL 150
    glsl_version = "#version 130";// changed from 150
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0); 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);//4
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);//3
#elif _WIN32
    // GL 3.0 + GLSL 130
    glsl_version = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0); 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#endif
    
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(
        SDL_WINDOW_OPENGL
        | SDL_WINDOW_RESIZABLE
        | SDL_WINDOW_ALLOW_HIGHDPI
        );
    SDL_Window *window = SDL_CreateWindow(
        "Sim 8088 [Code Enhance]",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        windowWidth,
        windowHeight,
        window_flags
        );
    // limit to which minimum size user can resize the window
    SDL_SetWindowMinimumSize(window, 500, 300);
    
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == NULL){
        std::cerr << "[ERROR] Failed to get a GL Context" << std::endl;
        std::cerr << SDL_GetError() << std::endl;
        return -1;
    }
    if (SDL_GL_MakeCurrent(window, gl_context)){
        std::cout << SDL_GetError() << std::endl;
        return -1;
    } else {
        std::cout << "[INFO] gl is current" << std::endl;
    }
    
    // enable VSync
    SDL_GL_SetSwapInterval(1);

    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
    {
        std::cerr << "[ERROR] Couldn't initialize glad" << std::endl;
    }
    else
    {
        std::cout << "[INFO] glad initialized\n";
    }

    std::cout << "[INFO] OpenGL renderer: "
              << glGetString(GL_RENDERER)
              << std::endl;

    // apparently, that shows maximum supported version
    std::cout << "[INFO] OpenGL from glad: "
              << GLVersion.major
              << "."
              << GLVersion.minor
              << std::endl;

    glViewport(0, 0, windowWidth, windowHeight);

    // setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.Fonts->AddFontFromFileTTF("verdana.ttf", 18.0f, NULL, NULL);
    ImFont *monoFont = io.Fonts->AddFontFromFileTTF("ti-83pl.ttf", 18.0f, NULL, NULL);

    // setup Dear ImGui style
    //ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();
    setImGuiStyle();

    // setup platform/renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version.c_str());

    bool show_demo_window = false;
    bool show_another_window = false;
    // colors are set in RGBA, but as float
    ImVec4 background = ImVec4(35/255.0f, 35/255.0f, 35/255.0f, 1.00f);

    glClearColor(background.x, background.y, background.z, background.w);
    // --- rendering loop
    bool loop = true;
    while (loop)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            // without it you won't have keyboard input and other things
            ImGui_ImplSDL2_ProcessEvent(&event);
            // you might also want to check io.WantCaptureMouse and io.WantCaptureKeyboard
            // before processing events

            switch (event.type)
            {
            case SDL_QUIT:
                loop = false;
                break;

            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_RESIZED:
                    windowWidth = event.window.data1;
                    windowHeight = event.window.data2;
                    glViewport(0, 0, windowWidth, windowHeight);
                    break;
                }
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    loop = false;
                    break;
                }
                break;
            }
        }


        // start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();

        static int counter = 0;
        static int showMyWindow = 0;
        static bool loaded = false;

        int sdl_width = 0, sdl_height = 0, controls_width = 0;
        // get the window size as a base for calculating widgets geometry
        SDL_GetWindowSize(window, &sdl_width, &sdl_height);
        controls_width = sdl_width;
        // make controls widget width to be 1/3 of the main window width
        if ((controls_width /= 3) < 300) { controls_width = 300; }

        // position the controls widget in the top-right corner with some margin
        ImGui::SetNextWindowPos(ImVec2(10, 10), ImGuiCond_Always);
        // here we set the calculated width and also make the height to be
        // be the height of the main window also with some margin
        ImGui::SetNextWindowSize(
            ImVec2(static_cast<float>(controls_width), static_cast<float>(sdl_height - 20)),
            ImGuiCond_Always
            );
 

        ImGui::Begin("Controls", NULL, ImGuiWindowFlags_NoResize);
        ImGui::InputText("Assembly File", env.filename, MAX_BUF_SZ);

        if (ImGui::Button("GO")){
            showMyWindow = true;
            Reset(&env);
            executeMachine = false;

            //Since a new program is loading do a hard reset.
            //
            //Having the old memory and reg values just make it hard to reason
            //about things.

            ResetMachine(&env.machine, true);
            loaded = false;
        }

        if (showMyWindow){
            ImGui::PushFont(monoFont);
            LoadCodeSegment(&env, &loaded);

            if (loaded){

                // CS will need to be a different display type since the data
                // offset will not always line up with the line display 
                OpenCodeSegmentDisplayWindow(&env); 
                
                OpenBinaryDisplayWindow(&env, REG_ES, 0xffff);
                OpenBinaryDisplayWindow(&env, REG_DS, 0xffff);
                OpenBinaryDisplayWindow(&env, REG_SS, 0xffff);

                ClearBreakpoints(&env.breakpoints);                    
                OpenDisassembleDisplayWindow(&env);

                if (initFramebuffer){
                    InitFramebuffer(&env, &framebufferWidth, &framebufferHeight);
                    initFramebuffer = false;
                }

                OpenFramebuffer(&env, framebufferWidth, framebufferHeight);

                OpenStackView(&env);
                
            }
            ImGui::PopFont();
        }

        ImGui::InputText("Save File", env.savefile, MAX_BUF_SZ);

        if (ImGui::Button("Save")){
            if (loaded){
                //TODO add clipping for Disassemble file output
                WriteDisassembleFile(&env);
                strncpy(env.message, "File Saved.", MAX_BUF_SZ);
            }
        }

#if 0
        ImGui::InputText("Dump Op Table", env.dumpfile, MAX_BUF_SZ);

        if (ImGui::Button("Dump")){
            DumpOpTable(&env);
            strncpy(env.message, "Table Dumped.", MAX_BUF_SZ);
        }
#endif

        ImGui::Checkbox("Hex Output", &env.hexDisplay);

        
        if (strnlen(env.message, MAX_BUF_SZ)){
            ImGui::Text(env.message);
        }

#if EVAL_ON
        ImGui::Dummy(ImVec2(0, 20));

        if (ImGui::Button("Reset")){
            //Soft Reset
            executeMachine = false;
            ResetMachine(&env.machine, false);
       }
 
        ImGui::SameLine();
        if (ImGui::Button("Next")){
            bool blitOccured = false;
            StepMachine(&env, &blitOccured); 
            env.machine.halt = false;
        }

        ImGui::SameLine();
        if (ImGui::Button("Continue")){
            env.machine.halt = false;
            executeMachine = true;
            //We don't want to get stuck in a inner loop
            ExecuteMachine(&env, &executeMachine, maxSteps);
        }

        ImGui::SameLine();
        ImGui::SetNextItemWidth(ImGui::GetFontSize() * 6);
        ImGui::SliderInt("Speed", &maxSteps, (uint32_t)(64 * 64 * 0.5), 64 * 64 * 10);

        //TODO add a break button to kill execution
        if (executeMachine){
            ExecuteMachine(&env, &executeMachine, maxSteps);
        }

        ImGui::Dummy(ImVec2(0, 50));

        ImGui::Columns(4);
        for(int i = 0; i < NUM_REGS/2; i++){
            ImGui::Text(regs[i], ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        for(int i = 0; i < NUM_REGS/2; i++){
            uint16_t val = env.machine.registers[i]; 
            ImGui::Text("0x%.4x", val, ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        for(int i = NUM_REGS/2; i < NUM_REGS; i++){
            ImGui::Text(regs[i], ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        for(int i = NUM_REGS/2; i < NUM_REGS; i++){
            uint16_t val = env.machine.registers[i]; 
            ImGui::Text("0x%.4x", val, ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        for(int i = 0; i < NUM_SEG_REGS; i++){
            ImGui::Text(segregs[i], ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        for(int i = 0; i < NUM_SEG_REGS; i++){
            uint16_t val = env.machine.segregs[i]; 
            ImGui::Text("0x%.4x", val, ImGui::GetColumnWidth());
            ImGui::NextColumn();
        }

        ImGui::Columns(1);
        ImGui::Dummy(ImVec2(0, 20));

        ImGui::Columns(2);
        ImGui::Text("IP");
        ImGui::NextColumn();
        ImGui::Text("0x%.4x", env.machine.pc);
        ImGui::NextColumn();
        ImGui::Text("Total Cycles");
        ImGui::NextColumn();
        ImGui::Text("%6d", env.totalCycles);
         
        ImGui::Columns(1);
        ImGui::Dummy(ImVec2(0, 50));
  
        ImGui::Columns(2);
        ImGui::Text("trap");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.trap);
        ImGui::NextColumn();
        ImGui::Text("direction");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.direction);
        ImGui::NextColumn();
        ImGui::Text("interruptEnable");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.interruptEnable);
        ImGui::NextColumn();
        ImGui::Text("overflow");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.overflow);
        ImGui::NextColumn();
        ImGui::Text("sign");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.sign);
        ImGui::NextColumn();
        ImGui::Text("zero");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.zero);
        ImGui::NextColumn();
        ImGui::Text("auxCarry");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.auxCarry);
        ImGui::NextColumn();
        ImGui::Text("parity");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.parity);
        ImGui::NextColumn();
        ImGui::Text("carry");
        ImGui::NextColumn();
        ImGui::Text("%d", env.machine.flags.carry);


        ImGui::Columns(1);
#endif
        for(int i = 0; i < 4; i++){
            if (env.dumpMemory[i]){
                DumpMemoryToFile(&env, &env.dumpMemory[i], (seg_reg_t)i);
            }
        }

        if (env.framebufferSettings.showing){
            OpenFramebufferSettings(&env);            
        }

        ImGui::End();

        // rendering
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        
        SDL_GL_SwapWindow(window);
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
