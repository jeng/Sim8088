void TestPaint(int x, int y, int r, int g, int b){
    int myaddr = 0x200 + (((64 * 4) * y) + x);

    asm push ax;
    asm push bx;
    asm push cx;
    asm mov ax, r;
    asm mov bx, g;
    asm mov cx, b;
    asm mov dx, myaddr;
    asm push bp;
    asm mov bp, dx;
    asm mov [bp], ax;
    asm mov [bp+1], bx;
    asm mov [bp+2], cx;
    asm mov [bp+3], byte 255;
    asm pop bp;
    asm pop cx;
    asm pop bx;
    asm pop ax;
    asm wait;

}

void main(){
    int i = 0;
    for(; i < 64; i++){
        TestPaint( 0, i, 255, 255, 0);
        TestPaint(63 * 4, i, 255, 0, 255);
    }
    asm wait;
}
