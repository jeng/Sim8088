#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

vector<string> LoadFile(string &filename){
    vector<string> v;
    string line;
    ifstream myfile;
    myfile.open(filename);
    if (myfile.is_open()){
        while(getline(myfile, line)){
            v.push_back(line);
        }
    }
    myfile.close();
    return v;
}

int SkipSpace(string &line, int &x){
    while(x < line.size() && (line[x] == ' ' || line[x] == '\t')){
        x++;
    }
    return x;
}

string GetVisible(string &line, int &x){
    string s = "";
    while(x <= line.size() && '!' <= line[x] && line[x] <= '~'){
        s += line[x++];
    }
    return s;
}

bool EndsWith(string &s, char c){
    if (s.size() > 0){
        return (s[s.size()-1] == c);
    }
    return false;
}

vector<string> GetTokens(string &line){
    vector<string> result;
    int x = 0;
    while(x < line.size()){
        string token = GetVisible(line, x);
        SkipSpace(line, x);
        if (token.size() > 0){
            result.push_back(token);
        }
    }
    return result;
}

string JoinString(vector<string> &tokens){
    string result;
    int n = 0;
    for(string x : tokens){

        if (x == "ptr"){
            n++;
            continue;
        }

        result += x;
        if (++n != tokens.size()){
            result += " ";
        }
    }
    return result;
}

bool KnownOp(string op){

    string ops[] = {"add", "adc", "sub", "sbb", "cmp", "jo", "jno", "jb",
        "jnb", "je", "jne", "jbe", "jnbe", "js", "jns", "jp", "jnp", "jl",
        "jge", "jle", "jg", "loopnz", "loopz", "loop", "push", "pop", "wait",
        "hlt", "call", "jmp", "ret", "cs", "ds", "es", "ss", "mov", "inc",
        "dec", "or", "xor", "and", "shl", "cbw"};   

    for(auto s : ops){
        //cout <<"db " << s << " " << op << endl;
        if (op == s)
            return true;
    }
    return false;
}

bool IsProc(vector<string> &tokens){
    if (tokens.size() > 1){
        return tokens[1] == "proc";
    }
    return false;
}

string ProcToLabel(vector<string> &tokens){
    return tokens[0] + ":";
}

void OutputFilter(vector<string> &tokens){
    if (tokens.size() == 1 && EndsWith(tokens[0], ':')){
        //Probably a label
        cout << JoinString(tokens) << endl;
    } else {
        if (tokens.size() >= 1 && KnownOp(tokens[0])){
            cout << JoinString(tokens) << endl;
        } else {
            if (IsProc(tokens)){
                cout << ProcToLabel(tokens) << endl;
            }
        }
    }
}

void UnknownFilter(vector<string> &tokens){
    if (tokens.size() == 1 && EndsWith(tokens[0], ':')){
        //Probably a label
        //cout << JoinString(tokens) << endl;
    } else {
        if (tokens.size() >= 1 && !KnownOp(tokens[0]) && tokens[0] != ";"){
            cout << JoinString(tokens) << endl;
        } else {
            //if (!IsProc(tokens)){
            //    cout << ProcToLabel(tokens) << endl;
            //}
        }
    }
}


void DebugToken(vector<string> &tokens){
    cout << "debug |";
    for(string x : tokens){
        cout << x << "|";
    }
    cout << endl;
}

int main(int argc, char **argv) {
    if (argc <= 1){
        cerr << "Pass in a filename" << endl;
    } else {
        string filename(argv[1]);
        vector<string> lines = LoadFile(filename);
        if (argc > 2){
            printf("len %d %s %d\n", strlen(argv[2]), argv[2], strcmp(argv[2], "-n"));
            if (strlen(argv[2]) >= 2 && strcmp(argv[2], "-n") == 0){
                for(string x : lines){
                    vector<string> token = GetTokens(x);
                    //DebugToken(token);
                    UnknownFilter(token);
                    //cout << x << endl;
                }
            }
        } else {
            string filename(argv[1]);
            //vector<string> lines = LoadFile(filename);
            cout << "bits 16" << endl;
            cout << "call _main" << endl;
            cout << "hlt" << endl;
            for(string x : lines){
                vector<string> token = GetTokens(x);
                //DebugToken(token);
                OutputFilter(token);
                //cout << x << endl;
            }
        }
        return 0;
    }
}
