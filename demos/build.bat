formatAsm check.asm > checkfm.asm
nasm checkfm.asm -o check.raw
nasm checkfm.asm -f bin -o check.com

nasm tstcon.asm -o tstcon.raw
nasm tstcon.asm -f bin -o tstcon.com

nasm subtst.asm -o subtst.raw
nasm subtst.asm -f bin -o subtst.com

formatAsm mmdbg.asm > mmdbgfm.asm
nasm mmdbgfm.asm -o mmdbg.raw
nasm mmdbgfm.asm -f bin -o mmdbg.com

nasm wrdcmpd.asm -o wrdcmpd.raw
nasm wrdcmpd.asm -f bin -o wrdcmpd.com

nasm scroll8086.asm -o scroll8086.raw
nasm scroll8086.asm -f bin -o scr8086.com
