#define s8 char
#define u8 unsigned char
#define u16 unsigned int
#define s16 int
#define FB_MAX (64)
#define FB_LOC (0x0)
#define PIX_SZ (4)
#define G_W (4)
#define G_H (4)
#define MAX(x, y) ((x) >= (y)) ? (x) : (y)
#define MIN(x, y) ((x) < (y)) ? (x) : (y)
#define BLIT() asm wait

void PutPixel(s16 x, s16 y, int r, int g, int b){
    int myaddr = FB_LOC + (((FB_MAX * PIX_SZ) * y) + x);
    if (x < 0) return;
    if (y < 0) return;
    if (x > (FB_MAX * PIX_SZ)) return;
    if (y > (FB_MAX)) return;

    asm push ax;
    asm push bx;
    asm push cx;
    asm mov ax, r;
    asm mov bx, g;
    asm mov cx, b;
    asm mov dx, myaddr;
    asm push bp;
    asm mov bp, dx;
    asm mov es:[bp], ax;
    asm mov es:[bp+1], bx;
    asm mov es:[bp+2], cx;
    asm mov es:[bp+3], byte 255;
    asm pop bp;
    asm pop cx;
    asm pop bx;
    asm pop ax;
}

/*TODO allow for end location*/
void VLine(s16 x, s16 y, u8 r, u8 g, u8 b){
    x *= PIX_SZ;
    for(; y < 64; y++){
        PutPixel(x, y, r, g, b);
    }
}

/*TODO allow for end location*/
void HLine(s16 x, s16 y, u8 r, u8 g, u8 b){
    for(; x < 64 * PIX_SZ; x+=4){
        PutPixel(x, y, r, g, b);
    }
}

void Square(s16 x, s16 y, s16 width, s16 height, u8 r, u8 g, u8 b){
    int xp, yp;
    int ends = PIX_SZ * (x + width);
    int sx = MAX(0, x * PIX_SZ);
    int ymin = MIN(y+height, FB_MAX);
    int xmin = MIN(ends, FB_MAX * PIX_SZ);
    for(yp = y; yp < ymin; yp++){
        for(xp = sx; xp < xmin; xp+=PIX_SZ){
            PutPixel(xp, yp, r, g, b);
        }
    }
}

void Clear(){
    int x = 0;
    int y = 0;
    for(; y < FB_MAX; y++){
        for(x=0; x < FB_MAX*PIX_SZ; x+=PIX_SZ){
            PutPixel(x, y, 0, 0, 0);
        }
    }
}

void main(){
    int x  = 0;
    int y  = 0;
    int xp = 0;
    /*
    Clear();
    Square(32, 8, 32, 32, 255, 140, 158);
    BLIT();
    */
    /*PutPixel(0, 0, 255,  100,  20);*/
    for(xp = 0; xp < FB_MAX; xp+=1){
        Clear();
        Square(xp, 8, 32, 32, 255, 140, 158);
        /*
        for(y = 0; y <= FB_MAX; y+=8){
            HLine(0, y, 100, 255, 100);
        }
        for(x = 0; x <= FB_MAX; x+=8){
            VLine(x, 0, 100, 255, 100);
        }
        HLine(0, FB_MAX-1, 100, 255, 100);
        VLine(FB_MAX-1, 0, 100, 255, 100);
        */
        BLIT();
    }

    /*while(1){*/
/*loop:*/
    /*
    Square(x,       0, 32, 32, 255,   0, 0);
    Square(x + 32,  0, 32, 32,   0,   0, 0);
    */
    /*Square(x,      32, 32, 32,   0,   0, 0);*/
    /*Square(x + 32, 32, 32, 32,   0, 255, 0);*/
    /*asm wait;*/
    /*
    x--;
    if (x == -32){
        x = 64;
    }
    goto loop;
    */
    /*}*/
    /*
       for(; x < 64;){
       Square(x, y, G_W, G_H, 200, 178, 200);
       x+=G_W;
       Square(x, y, G_W, G_H, 50, 100, 120);
       x+=G_W;
       }
       */
}
