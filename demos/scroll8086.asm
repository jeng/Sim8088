bits 16

; Constants
%define FB_MAX    64
%define FB_LOC     0
%define PIX_SZ     4
%define S_FB_MAX  PIX_SZ * FB_MAX

;Memory locations for data used by procedures
;TODO changed this to a stack based calling when
;     I move this code into an include library?
%define fillColor   0x600
%define progData    0x500
%define startX      progData
%define endX        startX + 2
%define startY      endX + 2
%define endY        startY + 2
%define rectW       endY + 2
%define rectH       rectW + 2
%define eight1_h_a  rectH + 2      ; history for the first 8 a reg
%define eight1_h_b  eight1_h_a + 2 ; history for the first 8 b reg
%define zero_h_a    eight1_h_b + 2 ; history for the 0 a reg
%define zero_h_b    zero_h_a + 2   ; history for the 0 b reg
%define eight2_h_a  zero_h_b + 2   ; history for the second 8 a reg
%define eight2_h_b  eight2_h_a + 2 ; history for the second 8 b reg
%define six_h_a     eight2_h_b + 2 ; history for the 6 a reg
%define six_h_b     six_h_a + 2    ; history for the 6 b reg
%define scratch     six_h_b + 2

; Macros
%macro Blit 0
    wait
%endmacro

%macro SetFillColor 3
    mov [fillColor    ], byte %1 
    mov [fillColor + 1], byte %2
    mov [fillColor + 2], byte %3
%endmacro

%macro SetPen 2
    mov [startX], word %1
    mov [startY], word %2 
%endmacro

%macro SetPenAB 0
    mov [startX], word ax
    mov [startY], word bx
%endmacro

%macro SetRectSize 2
    mov [rectW], word %1
    mov [rectH], word %2 
%endmacro

%macro SetPenBottomLeftAB 0
    mov [endX],   word ax
    mov [endY],   word bx
%endmacro

; Main
mov bp, progData
SetRectSize 32, 32
%define loopStart -96
mov ax, loopStart
mov bx, 10
mov cx, 0

SetFillColor 100, 140, 200
call PutBackground

main_loop:
    inner_loop:
        call drawLogo
        call clearLogo
        cmp cx, 0
        je movForward
        jmp movBackward
    movForward:
        inc ax
        cmp ax, FB_MAX
        jl inner_loop
        jmp exitLoop
    movBackward:
        ;dec ax
        sub ax, 1
        cmp ax, loopStart
        jg inner_loop
    exitLoop:
    cmp cx, 0
    je flipDir
    mov cx, 0
    jmp main_loop
    flipDir:
    mov cx, 1
    jmp main_loop
hlt


; Procedures

; Using the current fillColor and point locations set a pixel
PutPixel:
    ; store bp and ax since we change those
    push ax
    push bp
    push cx
    ; Is y in range
    mov ax, [startY]
    cmp ax, 0
    jl PutPixelExit
    cmp ax, FB_MAX
    jg PutPixelExit
    ; myAddr = FB_LOC + ((S_FB_MAX * y) + x)
    ; multiply by y
    mov cl, 8
    shl ax, cl 
    mov bp, ax
    ; Is x in range
    mov ax, [startX]
    cmp ax, 0
    jl PutPixelExit
    cmp ax, S_FB_MAX
    jg PutPixelExit
    ; Add x to the base pointer
    ; We can save an add by assuming FB_LOC = 0
    add bp, ax
    add bp, FB_LOC
    ; Paint
    mov ax, [fillColor]
    mov es:[bp], ax
    mov ax, [fillColor + 1]
    mov es:[bp + 1], ax
    mov ax, [fillColor + 2]
    mov es:[bp + 2], ax
    mov es:[bp + 3], byte 255 ; no alpha
   
    ; Restore values and exit
    PutPixelExit:
        pop cx
        pop bp
        pop ax
        ret

PutRect:
    ; TODO I should probably move these out so other don't over write
    %define ends scratch
    %define sx scratch + 2
    %define ymin scratch + 4
    %define xmin scratch + 6

    push ax
    push cx
    push bx

    ;ends = PIX_SZ * (x + width)
    mov ax, [startX]
    add ax, [rectW]
    mov cl, 2 ; Multiply by 4
    shl ax, cl
    mov [ends], word ax    
    ;sx = MAX(0, x * PIX_SZ)
    mov ax, [startX]
    shl ax, cl ; TODO what happens when we shift left a negative? (-127)
    cmp ax, 0
    jg isMax
    xor ax, ax
isMax:
    mov [sx], word ax
    ;ymin = MIN(y+height, FB_MAX)
    mov ax, [startY]
    add ax, [rectH]
    cmp ax, FB_MAX
    jl isMin1
    mov ax, FB_MAX
isMin1:
    mov [ymin], word ax
    ;int xmin = MIN(ends, FB_MAX * PIX_SZ);
    mov ax, [ends]
    cmp ax, S_FB_MAX
    jl isMin2
    mov ax, S_FB_MAX
isMin2:
    mov [xmin], word ax
    ;Paint the rectangle
    mov bx, [startY]
    rect_loop1:
        mov ax, [sx]
        rect_loop2:
            SetPenAB
            call PutPixel
            add ax, PIX_SZ
            cmp ax, [xmin]
            jl rect_loop2
        inc bx
        cmp bx, [ymin]
        jl rect_loop1
    
    pop bx
    pop cx
    pop ax
    ret

PutBackground:
    push ax
    push bx
    mov bx, 0
    background_loop1:
        mov ax, 0
        background_loop2:
            SetPenAB
            call PutPixel
            add ax, PIX_SZ
            cmp ax, S_FB_MAX
            jl background_loop2
        inc bx
        cmp bx, FB_MAX
        jl background_loop1
    pop bx
    pop ax    
    ret

PutEight:
    push ax
    push bx

    SetRectSize 5, 35
    call PutRect
    push ax
    add ax, 22
    SetPenAB
    call PutRect
    pop ax
    SetPenAB

    SetRectSize 22, 5 
    call PutRect
    add bx, 15 
    SetPenAB
    call PutRect
    add bx, 15
    SetPenAB
    call PutRect


    pop bx
    pop ax
    ret

PutZero:
    push ax
    push bx

    SetRectSize 5, 35
    call PutRect
    push ax
    add ax, 22
    SetPenAB
    call PutRect
    pop ax
    SetPenAB

    SetRectSize 22, 5 
    call PutRect
    add bx, 30 
    SetPenAB
    call PutRect

    pop bx
    pop ax
    ret

PutSix:
    push ax
    push bx

    SetRectSize 5, 35
    call PutRect

    SetRectSize 22, 5 
    add bx, 15 
    SetPenAB
    call PutRect


    SetRectSize 5, 20
    push ax
    add ax, 22
    SetPenAB
    call PutRect
    pop ax


    SetRectSize 22, 5 
    add bx, 15
    SetPenAB
    call PutRect
 
    pop bx
    pop ax
    ret

drawLogo:
    ; Get a sin value for y
    mov di, ax
    add di, -1 * loopStart
    xor bx, bx
    mov bl, cs:sinTable[di] 

    SetPenAB
    mov [eight1_h_a], ax
    mov [eight1_h_b], bx
    SetFillColor 255, 140, 158
    call PutEight

    push ax
    add ax, 30
    SetPenAB
    mov [zero_h_a], ax
    mov [zero_h_b], bx
    call PutZero

    add ax, 30
    SetPenAB
    mov [eight2_h_a], ax
    mov [eight2_h_b], bx
    call PutEight

    add ax, 30
    SetPenAB
    mov [six_h_a], ax
    mov [six_h_b], bx
    call PutSix

    pop ax
    Blit
    ret

clearLogo:
    ;; Clear previous font instead of full redraw
    SetFillColor 100, 140, 200
    push ax
    push bx
    
    mov ax, [eight1_h_a]
    mov bx, [eight1_h_b]
    SetPenAB
    call PutEight
    
    mov ax, [zero_h_a]
    mov bx, [zero_h_b]
    SetPenAB
    call PutZero
    
    mov ax, [eight2_h_a]
    mov bx, [eight2_h_b]
    SetPenAB
    call PutEight
    
    mov ax, [six_h_a]
    mov bx, [six_h_b]
    SetPenAB
    call PutSix

    pop bx
    pop ax

    ret 


sinTable:
db 21, 22, 22, 23, 23, 24, 24, 24, 24, 24, 24, 24, 23, 23, 22, 22, 21, 21, 20, 19, 18, 18, 17, 16, 16, 15, 14, 14, 13, 13, 12, 12, 12, 12, 12, 12, 12, 13, 13, 14, 14, 15, 15, 16, 17, 17, 18, 19, 20, 20, 21, 22, 22, 23, 23, 23, 24, 24, 24, 24, 24, 24, 23, 23, 23, 22, 21, 21, 20, 19, 19, 18, 17, 16, 16, 15, 14, 14, 13, 13, 13, 12, 12, 12, 12, 12, 12, 13, 13, 13, 14, 14, 15, 16, 17, 17, 18, 19, 19, 20, 21, 22, 22, 23, 23, 23, 24, 24, 24, 24, 24, 24, 23, 23, 23, 22, 22, 21, 20, 20, 19, 18, 17, 17, 16, 15, 15, 14, 13, 13, 13, 12, 12, 12, 12, 12, 12, 13, 13, 13, 14, 14, 15, 16, 16, 17, 18, 19, 19, 20, 21, 21, 22, 22, 23, 23, 24, 24, 24, 24
