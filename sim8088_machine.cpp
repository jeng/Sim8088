enum logic_t{
    LT_AND, LT_OR, LT_XOR
};

void ResetMachine(sim86_machine_t *m86, bool hard){
    m86->flags.trap = false;
    m86->flags.direction = false;
    m86->flags.interruptEnable = false;
    m86->flags.overflow = false;
    m86->flags.sign = false;
    m86->flags.zero = false;
    m86->flags.auxCarry = false;
    m86->flags.parity = false;
    m86->flags.carry = false;
    m86->halt = false;
    m86->activeSegment = REG_DS;
    m86->pc = 0;
    m86->registers[REG_SP] = 0xfffc;
    
    //TODO do we want to do this on a soft reset?
    m86->registers[REG_AX] = 0;
    m86->registers[REG_BX] = 0;
    m86->registers[REG_CX] = 0;
    m86->registers[REG_DX] = 0;

    if (hard){
        if (m86->memory == NULL){
            m86->memory = (uint8_t*)calloc(MAX_MEM, sizeof(uint8_t));    
        } else {
            memset(m86->memory, 0, MAX_MEM); 
        }
    }
}

sim86_machine_t BuildMachine(){
    sim86_machine_t m86 = {0};
    ResetMachine(&m86, true);
    return m86;       
}

bool IsLowRegister(operand_t *operand){
    return (!operand->isWord && operand->reg < 4);
}

bool IsHighRegister(operand_t *operand){
    return (!operand->isWord && operand->reg >= 4);
}

uint16_t GetRegValue(sim86_machine_t *machine, operand_t *operand){
    if (IsLowRegister(operand)){
        return machine->registers[operand->reg] & 0x00ff;
    } else if (IsHighRegister(operand)) {
        uint8_t reg = operand->reg - 4;
        return (machine->registers[reg] & 0xff00) >> 8;
    } else{
        return machine->registers[operand->reg];
    }
}

void SetRegValue(sim86_machine_t *machine, operand_t *operand, uint16_t value){
    if (IsLowRegister(operand)){
        machine->registers[operand->reg] = (machine->registers[operand->reg] & 0xff00) | (value & 0x00ff);
    } else if (IsHighRegister(operand)) {
        uint8_t reg = operand->reg - 4;
        machine->registers[reg] = (machine->registers[reg] & 0x00ff) | ((value << 8) & 0xff00);
    } else {
        machine->registers[operand->reg] = value;
    }
}

uint16_t GetSegRegValue(sim86_machine_t *machine, operand_t *operand){
    return machine->segregs[operand->segreg];
}

void SetSegRegValue(sim86_machine_t *machine, operand_t *operand, uint16_t value){
    machine->segregs[operand->segreg] = value;
}

uint32_t GetPhysicalAddress(sim86_machine_t *machine, operand_t *operand){
    uint32_t effectiveAddress = 0;

    if (operand->effectiveRegSize == 1){
        effectiveAddress = machine->registers[operand->effectiveReg[0]] + operand->effectiveOffset;
    } else if (operand->effectiveRegSize == 2){
        effectiveAddress = 
            machine->registers[operand->effectiveReg[0]] +
            machine->registers[operand->effectiveReg[1]] +
            operand->effectiveOffset;
    } else {
        effectiveAddress = operand->address;
    }

    effectiveAddress = (machine->segregs[machine->activeSegment] << 4) + effectiveAddress;

    return effectiveAddress;
}

uint16_t GetMemoryValue(sim86_machine_t *machine, operand_t *operand, bool isWord){
    uint32_t effectiveAddress = GetPhysicalAddress(machine, operand);
    uint16_t result;
    machine->activeSegment = REG_DS;

    if (isWord){
        result = machine->memory[effectiveAddress + 1];
        result <<= 8;
        result |= machine->memory[effectiveAddress];
    } else {
        result = machine->memory[effectiveAddress];
    }

    return result;        
}

uint16_t MSB(uint16_t value, bool isWord){
    uint16_t high;
    if (isWord){
        high = (value & 0x8000) >> 15;
    } else {
        high = (value & 0x80) >> 7;
    }
    return high;
}

uint16_t LSB(uint16_t value){
    return (value & 0x1);
}

void SetMemoryValue(sim86_machine_t *machine, operand_t *operand, uint16_t value, bool isWord){
    uint32_t effectiveAddress = GetPhysicalAddress(machine, operand);
    machine->activeSegment = REG_DS;
    if (isWord){
        machine->memory[effectiveAddress] = value & 0xff;
        machine->memory[effectiveAddress + 1] = (value & 0xff00) >> 8;
    } else {
        machine->memory[effectiveAddress] = value & 0xff;
    }
}

uint16_t GetValueByOperandType(sim86_machine_t *machine, operand_t *operand, bool isWord){
    uint16_t value = 0;
    if (operand->type == OT_IMMEDIATE){
        value = operand->immediate;
    } else if (operand->type == OT_REGISTER){
        value = GetRegValue(machine, operand);
    } else if (operand->type == OT_SEG_REG){
        value = GetSegRegValue(machine, operand);
    } else if (operand->type == OT_ADDRESS){
        value = GetMemoryValue(machine, operand, isWord);
    } else {
        fprintf(stderr, "Not implemented\n");
        return 0;
    }
    return value;
}

int16_t GetSignedValueByOperandType(sim86_machine_t *machine, operand_t *operand, bool isWord){
    uint16_t value = GetValueByOperandType(machine, operand, isWord);
    if (isWord){
        int16_t x = value;
        return x;
    } else {
        int8_t x = (int8_t)value;
        return x;
    }
}

void SetValueByOperandType(sim86_machine_t *machine, operand_t *operand, uint16_t value, bool isWord){
    if (operand->type == OT_IMMEDIATE){
        fprintf(stderr, "Cannot assign to an immediate\n");         
    } else if (operand->type == OT_REGISTER){
        SetRegValue(machine, operand, value);
    } else if (operand->type == OT_SEG_REG){
        SetSegRegValue(machine, operand, value);
    } else if (operand->type == OT_ADDRESS){
        SetMemoryValue(machine, operand, value, isWord);
    } else {
        fprintf(stderr, "Not implemented\n");
    }
}

uint16_t CalculateFullAddress(sim86_machine_t *machine, operand_t *operand){
    uint8_t segValue = (uint8_t)machine->segregs[REG_DS];
    //Get Segment value based on the operand type
    if (operand->type == OT_SEG_REG){
        segValue = (uint8_t)machine->segregs[operand->segreg];
        return (segValue * 16) + operand->displacement;
    }
    return (segValue * 16) + operand->address;
}

//TODO make sure that this is correct
//TODO kinda lame duplicating the operation here
bool IsAuxCarry(int32_t left, int32_t right, bool isCarry, bool isAdd){
    uint16_t x = left;
    uint16_t y = right;
    x &= 0xf;
    y &= 0xf;
    if (isAdd){
        x += y;
        if (isCarry){
            x++;
        }
    } else {
        x -= y;
        if (isCarry){
            x--;
        }
    }
    return (x > 0xf);
}

bool IsEvenParity(int32_t value){
    uint8_t x = value & 0xff;
    int on = 0;
    for(int i = 0; i < 8; i++){
        if (x & 1){
            on++;
        }
        x >>= 1;
    }
    return (on % 2 == 0);
}

//bool IsOverflow(int32_t value, bool is16bit){
//    if (is16bit){
//        return (value < SHRT_MIN || value > SHRT_MAX); 
//    } else {
//        return (value < CHAR_MIN || value > CHAR_MAX);
//    }
//}

bool IsCarry(uint32_t value, bool is16bit){
    if (is16bit){
        return (value > USHRT_MAX);
    } else {
        return (value > UCHAR_MAX);
    }
}

bool IsSign(int32_t value, bool is16bit){
    if (is16bit){
        int16_t x = value;
        return (x < 0);
    } else {
        int8_t x = value;
        return (x < 0);
    }
}

void MovEval(sim86_machine_t *machine, ins_8088_t *ins){
    //TODO this is weird... if we say move byte to word we want to get a byte
    //TODO this one has still got me guessing.  I've added the OR for isWord
    //     I need to setup some test cases with different move sizes or read
    //     the manual closer
    uint16_t value = GetValueByOperandType(machine, 
            &ins->src, 
            ins->dest.isWord || ins->src.isWord);

    if (ins->dest.type == OT_REGISTER){
        SetRegValue(machine, &ins->dest, value);
    } else if (ins->dest.type == OT_SEG_REG){
        SetSegRegValue(machine, &ins->dest, value);
    } else if (ins->dest.type == OT_ADDRESS){
        SetMemoryValue(machine, &ins->dest, value, ins->src.isWord);
    }
}

int32_t DoArithmetic(sim86_machine_t *machine, ins_8088_t *ins, bool withCarry, bool isAdd){
    uint32_t right = GetValueByOperandType(machine, &ins->src, ins->src.isWord);
    uint32_t left = GetValueByOperandType(machine, &ins->dest, ins->dest.isWord);
    uint32_t result;
    bool sub = !isAdd;

    //TODO now that I'm changing to two's complement on sub does this need to change?
    machine->flags.auxCarry = IsAuxCarry(left, right, machine->flags.carry, isAdd);

    uint8_t leftSignIn = 0; 
    uint8_t rightSignIn = 0; 
    uint8_t signOut = 0; 
    uint8_t carry = (isAdd) ? 1 : -1;
    if (ins->dest.isWord){
        left &= 0xffff;
        right &= 0xffff;
        if (sub)
            right = ~right + 1;
        leftSignIn = (uint8_t)MSB(left, true);
        rightSignIn = (uint8_t)MSB(right, true);
    } else {
        left &= 0xff;
        right &= 0xff;
        if (sub)
            right = ~right+1;
        leftSignIn = (uint8_t)MSB(left, false);
        rightSignIn = (uint8_t)MSB(right, false);
    }

    result = left + right;

    if (withCarry && machine->flags.carry)
        result+= carry; 

    //TODO Test subtract with carry

    //if (isAdd){ 
    //    result = left + right;
    //    if (withCarry && machine->flags.carry)
    //        result++; 
    //} else {
    //    result = left - right;
    //    if (withCarry && machine->flags.carry)
    //        result--; 
    //}

    machine->flags.sign = IsSign(result, ins->dest.isWord);
    machine->flags.carry = IsCarry(result, ins->dest.isWord);


    if (ins->dest.isWord){
        result &= 0xffff;
        signOut = (uint8_t)MSB(result, true);
    } else {
        result &= 0xff;
        signOut = (uint8_t)MSB(result, false);
    }

    //We might be able to break this out later
    machine->flags.parity = IsEvenParity(result);
    machine->flags.zero = (result == 0);
    //machine->flags.overflow = carry ^ sub;
    machine->flags.overflow = (!(leftSignIn ^ rightSignIn)) & (leftSignIn ^ signOut);

    return result;
}

//Logic enum will get passed in
int32_t DoLogic(sim86_machine_t *machine, ins_8088_t *ins, logic_t logic){
    uint32_t right = GetValueByOperandType(machine, &ins->src, ins->src.isWord);
    uint32_t left = GetValueByOperandType(machine, &ins->dest, ins->dest.isWord);
    uint32_t result;

    //TODO now that I'm changing to two's complement on sub does this need to change?
    //TODO do we need to pass true here?
    machine->flags.auxCarry = IsAuxCarry(left, right, machine->flags.carry, true);

    uint8_t leftSignIn = 0; 
    uint8_t rightSignIn = 0; 
    uint8_t signOut = 0; 
    uint8_t carry = 1;
    if (ins->dest.isWord){
        left &= 0xffff;
        right &= 0xffff;
        leftSignIn = (uint8_t)MSB(left, true);
        rightSignIn = (uint8_t)MSB(right, true);
    } else {
        left &= 0xff;
        right &= 0xff;
        leftSignIn = (uint8_t)MSB(left, false);
        rightSignIn = (uint8_t)MSB(right, false);
    }

    //TODO did this once I have the op enum in place
    switch(logic){
        case LT_XOR: result = left ^ right; break;
        case LT_AND: result = left & right; break;
        case LT_OR : result = left | right; break;
        default:
             assert(false);
             break;
    }

    //if (withCarry && machine->flags.carry)
    //    result+= carry; 

    machine->flags.sign = IsSign(result, ins->dest.isWord);
    machine->flags.carry = IsCarry(result, ins->dest.isWord);

    if (ins->dest.isWord){
        result &= 0xffff;
        signOut = (uint8_t)MSB(result, true);
    } else {
        result &= 0xff;
        signOut = (uint8_t)MSB(result, false);
    }

    //We might be able to break this out later
    machine->flags.parity = IsEvenParity(result);
    machine->flags.zero = (result == 0);
    machine->flags.overflow = (!(leftSignIn ^ rightSignIn)) & (leftSignIn ^ signOut);

    return result;
}



void AddEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
            &ins->dest, 
            DoArithmetic(machine, ins, false, true), 
            ins->dest.isWord);
}

void AdcEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
            &ins->dest, 
            DoArithmetic(machine, ins, false, true),
            ins->dest.isWord);
}

void SubEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
            &ins->dest, 
            DoArithmetic(machine, ins, true, false),
            ins->dest.isWord);
}

void SbbEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
            &ins->dest, 
            DoArithmetic(machine, ins, false, false),
            ins->dest.isWord);
}

void CmpEval(sim86_machine_t *machine, ins_8088_t *ins){
    DoArithmetic(machine, ins, false, false);
}

void UpdatePC(sim86_machine_t *machine, ins_8088_t *ins){
    assert(ins->dest.type == OT_IMMEDIATE);
    machine->pc += ins->dest.immediate;
}

void JnbEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.zero == 0 && machine->flags.carry == 0){
        UpdatePC(machine, ins);
    }
}

void JnbeEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.carry == 0){
        UpdatePC(machine, ins);
    }
}

void JoEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.overflow){
        UpdatePC(machine, ins);
    }
}

void JnoEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (!machine->flags.overflow){
        UpdatePC(machine, ins);
    }
}

void JbEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.carry){
        UpdatePC(machine, ins);
    }
}

void JeEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.zero){
        UpdatePC(machine, ins);
    }
}

void JneEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (!machine->flags.zero){
        UpdatePC(machine, ins);
    }
}

void JbeEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.zero || machine->flags.carry){
        UpdatePC(machine, ins);
    }
}

void JsEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.sign){
        UpdatePC(machine, ins);
    }
}

void JnsEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (!machine->flags.sign){
        UpdatePC(machine, ins);
    }
}

void JpEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.parity){
        UpdatePC(machine, ins);
    }
}

void JnpEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (!machine->flags.parity){
        UpdatePC(machine, ins);
    }
}

void JlEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.sign != machine->flags.overflow){
        UpdatePC(machine, ins);
    }
}

void JgeEval(sim86_machine_t *machine, ins_8088_t *ins){
    if (machine->flags.sign == machine->flags.overflow){
        UpdatePC(machine, ins);
    }
}

void JleEval(sim86_machine_t *machine, ins_8088_t *ins){
    if ((machine->flags.sign != machine->flags.overflow) || 
        (machine->flags.zero)) {
        UpdatePC(machine, ins);
    }
}

void JgEval(sim86_machine_t *machine, ins_8088_t *ins){
    if ((machine->flags.sign == machine->flags.overflow) && 
        (!machine->flags.zero)) {
        UpdatePC(machine, ins);
    }
}

void LoopnzEval(sim86_machine_t *machine, ins_8088_t *ins){
    machine->registers[REG_CX]--;
    if (machine->registers[REG_CX] != 0 && !machine->flags.zero) {
        UpdatePC(machine, ins);
    }
}

void LoopzEval(sim86_machine_t *machine, ins_8088_t *ins){
    machine->registers[REG_CX]--;
    if (machine->registers[REG_CX] != 0 && machine->flags.zero) {
        UpdatePC(machine, ins);
    }
}

void LoopEval(sim86_machine_t *machine, ins_8088_t *ins){
    machine->registers[REG_CX]--;
    if (machine->registers[REG_CX] != 0) {
        UpdatePC(machine, ins);
    }
}

void PushEval(sim86_machine_t *machine, ins_8088_t *ins){
    uint16_t value = GetValueByOperandType(machine, &ins->dest, ins->dest.isWord);
    machine->activeSegment = REG_SS;
    ins->dest.effectiveOffset = 0;
    ins->dest.effectiveRegSize = 1;
    ins->dest.effectiveReg[0] = REG_SP;
    machine->registers[REG_SP] -= 2;
    SetMemoryValue(machine, &ins->dest, value, true);
}

void PopEval(sim86_machine_t *machine, ins_8088_t *ins){
    ins->src.isWord = true;
    machine->activeSegment = REG_SS;
    ins->src.effectiveOffset = 0;
    ins->src.effectiveRegSize = 1;
    ins->src.effectiveReg[0] = REG_SP;
    uint16_t value = GetMemoryValue(machine, &ins->src, ins->src.isWord);
    machine->registers[REG_SP] += 2;
    SetValueByOperandType(machine, &ins->dest, value, true); 
}

void PrefixEval(sim86_machine_t *machine, ins_8088_t *ins){
    //TODO the machine will store the current prefix and clear it after each memory access
    //     This will work for the memory segments but will needed to be handled differently
    //     for other prefixes such as lock

    if (ins->src.type == OT_SEG_REG){
        machine->activeSegment = ins->src.segreg;
    } else if (ins->dest.type == OT_SEG_REG){
        machine->activeSegment = ins->dest.segreg;
    }
}

void WaitEval(sim86_machine_t *machine, ins_8088_t *ins){
    //"WAIT causes the CPU to enter the wait state while its test line is not
    //active."  jhe: I assume they mean pulled low?
    //
    //I'm using wait to trigger a blit
    machine->blit = true;
}

void JmpEval(sim86_machine_t *machine, ins_8088_t *ins){
    UpdatePC(machine, ins);
}

void CallEval(sim86_machine_t *machine, ins_8088_t *ins){
    //TODO implement the other call types
    uint16_t value = machine->pc + ins->length;

    //Push the return address onto the stack
    machine->activeSegment = REG_SS;
    ins->dest.effectiveOffset = 0;
    ins->dest.effectiveRegSize = 1;
    ins->dest.effectiveReg[0] = REG_SP;
    machine->registers[REG_SP] -= 2;
    SetMemoryValue(machine, &ins->dest, value, true);

    UpdatePC(machine, ins);
}

void RetEval(sim86_machine_t *machine, ins_8088_t *ins){
    ins->src.isWord = true;
    machine->activeSegment = REG_SS;
    ins->src.effectiveOffset = 0;
    ins->src.effectiveRegSize = 1;
    ins->src.effectiveReg[0] = REG_SP;
    uint16_t value = GetMemoryValue(machine, &ins->src, ins->src.isWord);
    machine->registers[REG_SP] += 2;
    machine->pc = value; 
    machine->pc--;//offset for the current ins
}

void HaltEval(sim86_machine_t *machine, ins_8088_t *ins){
    machine->halt = true;
}

void MulEval(sim86_machine_t *machine, ins_8088_t *ins){
    uint32_t left = GetValueByOperandType(machine, &ins->dest, ins->dest.isWord);
    uint32_t right = machine->registers[REG_AX];

    if (ins->dest.isWord){
        left &= 0xffff;
        right &= 0xffff;
    } else {
        left &= 0xff;
        right &= 0xff;
    }

    uint32_t value = left * right;

    if (ins->dest.isWord){
        machine->registers[REG_AX] = value & 0xffff;
        machine->registers[REG_DX] = (value & 0xffff0000) >> 16;
        machine->flags.overflow = (machine->registers[REG_DX] != 0);
        machine->flags.carry = (machine->registers[REG_DX] != 0);
    } else {
        machine->registers[REG_AX] = value & 0xffff;
        machine->flags.overflow = (machine->registers[REG_AX] > 0xff);
        machine->flags.carry = (machine->registers[REG_AX] > 0xff);
    }
}

void IncEval(sim86_machine_t *machine, ins_8088_t *ins){
    ins->src.type = OT_IMMEDIATE;
    ins->src.immediate = 1;
    SetValueByOperandType(machine, 
           &ins->dest, 
           DoArithmetic(machine, ins, false, true),
           ins->dest.isWord);
}

void XorEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
           &ins->dest, 
           DoLogic(machine, ins, LT_XOR),
           ins->dest.isWord);
}

void AndEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
           &ins->dest, 
           DoLogic(machine, ins, LT_AND),
           ins->dest.isWord);
}

void OrEval(sim86_machine_t *machine, ins_8088_t *ins){
    SetValueByOperandType(machine, 
           &ins->dest, 
           DoLogic(machine, ins, LT_OR),
           ins->dest.isWord);
}

void ShlEval(sim86_machine_t *machine, ins_8088_t *ins){
    //TODO move out to another func and add shift right stuff
    uint16_t count = GetValueByOperandType(machine, &ins->src, ins->src.isWord);
    uint16_t dest  = GetValueByOperandType(machine, &ins->dest, ins->dest.isWord);

    uint16_t temp = count;
    while (temp != 0){
        machine->flags.carry = MSB(dest, ins->dest.isWord);
        dest = dest * 2;
        temp--;
    }
    if (count == 1){
        machine->flags.overflow = MSB(dest, ins->dest.isWord) ^ (uint16_t)machine->flags.carry;
    }
    //else if (count == 0){
    //}
    //

    SetValueByOperandType(machine, &ins->dest, dest, ins->dest.isWord);
}

void CbwEval(sim86_machine_t *machine, ins_8088_t *ins){
    uint16_t ax = machine->registers[REG_AX];
    uint16_t al = (ax & 0xff00) >> 8;
    if (al < 0x80){
        ax = (al << 8);
    } else {
        ax = (al << 8) | 0xff;
    }
}

