static cycle_info_t cycleInfo[] = {
    {OP_MOV, 11, {
                 {OT_EFFECTIVE, OT_REGISTER,  false, CYCLE_ACCUMULATOR | 10, 1},
                 {OT_REGISTER,  OT_EFFECTIVE, false, CYCLE_ACCUMULATOR | 10, 1},
                 {OT_REGISTER,  OT_REGISTER,  false, 2, 0},
                 {OT_REGISTER,  OT_EFFECTIVE, false, CYCLE_EA | 8, 1},
                 {OT_EFFECTIVE, OT_REGISTER,  false, CYCLE_EA | 9, 1},
                 {OT_REGISTER,  OT_IMMEDIATE, false, 4, 0},
                 {OT_EFFECTIVE, OT_IMMEDIATE, false, CYCLE_EA | 10, 1},
                 {OT_SEG_REG,   OT_REGISTER,  false, 2, 0},
                 {OT_SEG_REG,   OT_EFFECTIVE, false, CYCLE_EA | 8, 1},
                 {OT_REGISTER,  OT_SEG_REG,   false, 2, 0},
                 {OT_EFFECTIVE, OT_SEG_REG,   false, CYCLE_EA | 9, 1}}},
    {OP_ADD, 6, {
                 {OT_REGISTER,  OT_REGISTER,  false, 3, 0},
                 {OT_REGISTER,  OT_EFFECTIVE, false, CYCLE_EA | 9, 1},
                 {OT_EFFECTIVE, OT_REGISTER,  false, CYCLE_EA | 16, 2},
                 {OT_REGISTER,  OT_IMMEDIATE, false, 4, 0},
                 {OT_EFFECTIVE, OT_IMMEDIATE, false, CYCLE_EA | 17, 2},
                 {OT_REGISTER,  OT_IMMEDIATE, false, CYCLE_ACCUMULATOR |4, 0}}}
};

/*For the 8086, add four clocks for each 16-bit word transfer with an odd address*/

uint16_t EffectiveCycles(operand_t *operand, cycle_details_t *detail){
    uint16_t penalty = 0;

    if (operand->isWord && 
        ((operand->effectiveOffset % 2 == 1) ||
         (operand->address % 2 == 1))){
        penalty = 4 * detail->transfers;
    }

    

    if (operand->effectiveRegSize == 0)
        return 6 + penalty;

    if (operand->effectiveRegSize == 1){
        if (operand->effectiveOffset == 0){
            return 5+ penalty;
        } else {
            return 9+ penalty;
        }
    } else {
        if (operand->effectiveOffset == 0){
            if (operand->effectiveReg[0] == REG_BP && operand->effectiveReg[1] == REG_DI)
                return 7+ penalty;
            if (operand->effectiveReg[0] == REG_DI && operand->effectiveReg[1] == REG_BP)
                return 7+ penalty;
            if (operand->effectiveReg[0] == REG_BX && operand->effectiveReg[1] == REG_SI)
                return 7+ penalty;
            if (operand->effectiveReg[0] == REG_SI && operand->effectiveReg[1] == REG_BX)
                return 7+ penalty;

            if (operand->effectiveReg[0] == REG_BP && operand->effectiveReg[1] == REG_SI)
                return 8+ penalty;
            if (operand->effectiveReg[0] == REG_SI && operand->effectiveReg[1] == REG_BP)
                return 8+ penalty;
            if (operand->effectiveReg[0] == REG_BX && operand->effectiveReg[1] == REG_DI)
                return 8+ penalty;
            if (operand->effectiveReg[0] == REG_DI && operand->effectiveReg[1] == REG_BX)
                return 8+ penalty;
        } else {
            if (operand->effectiveReg[0] == REG_BP && operand->effectiveReg[1] == REG_DI)
                return 11+ penalty;
            if (operand->effectiveReg[0] == REG_DI && operand->effectiveReg[1] == REG_BP)
                return 11+ penalty;
            if (operand->effectiveReg[0] == REG_BX && operand->effectiveReg[1] == REG_SI)
                return 11+ penalty;
            if (operand->effectiveReg[0] == REG_SI && operand->effectiveReg[1] == REG_BX)
                return 11+ penalty;

            if (operand->effectiveReg[0] == REG_BP && operand->effectiveReg[1] == REG_SI)
                return 12+ penalty;
            if (operand->effectiveReg[0] == REG_SI && operand->effectiveReg[1] == REG_BP)
                return 12+ penalty;
            if (operand->effectiveReg[0] == REG_BX && operand->effectiveReg[1] == REG_DI)
                return 12+ penalty;
            if (operand->effectiveReg[0] == REG_DI && operand->effectiveReg[1] == REG_BX)
                return 12+ penalty;
        }
    }
    return 0;
}

uint16_t GetCycleCount(ins_8088_t *ins){
    //This is probably fast enough using linear search but could be sped up
    //using the enum.
    int idx = -1;
    for(int i = 0; i < ARRAY_SIZE(cycleInfo); i++){
        if (cycleInfo[i].mnemonic == ins->mnemonic){
            idx = i;
            break;
        }
    }

    if (idx == -1)
        return 0;

    //Find the matching details
    operand_type_t srcType = ins->src.type;
    operand_type_t destType = ins->dest.type;

    if (srcType == OT_ADDRESS)
        srcType = OT_EFFECTIVE;

    if (destType == OT_ADDRESS)
        destType = OT_EFFECTIVE;

    for(int i = 0; i < cycleInfo[idx].detailCount; i++){
        cycle_details_t *detail = &cycleInfo[idx].details[i];
        if (detail->src == srcType && detail->dest == destType){
            //Determine if it is specific to accumulators and this is an accumulator
            if (detail->cycles & CYCLE_ACCUMULATOR) {

                if ((srcType  == OT_REGISTER && ins->src.reg  == REG_AX) || 
                    (srcType  == OT_REGISTER && ins->src.reg  == REG_AL) ||
                    (srcType  == OT_REGISTER && ins->src.reg  == REG_AH) ||
                    (destType == OT_REGISTER && ins->dest.reg == REG_AX) ||
                    (destType == OT_REGISTER && ins->dest.reg == REG_AL) || 
                    (destType == OT_REGISTER && ins->dest.reg == REG_AH)){

                    //TODO do we every have a better cycle time when an
                    //accumulator and effective address?
                        return (detail->cycles & CYCLE_MASK);
                }

            } else if (detail->cycles & CYCLE_EA){
                if (detail->src == OT_EFFECTIVE){
                    return (detail->cycles & CYCLE_MASK) + EffectiveCycles(&ins->src, detail);
                } else {
                    return (detail->cycles & CYCLE_MASK) + EffectiveCycles(&ins->dest, detail);
                }
            } else {
                //TODO Handle CYCLE_CHECK_WORD
                return detail->cycles & CYCLE_MASK;
            }
        }
    }

    return 0;
}
